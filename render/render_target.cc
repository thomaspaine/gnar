// Thomas Paine, 2024

#include "render_target.h"

namespace gnar {
  namespace render {
    RenderTarget::RenderTarget(core::Device &device, core::Swapchain &swapchain) :
      device{device},
      swapchain{swapchain} {
      spdlog::info("Creating render target");

      // Depth
      depth_format = device.get_depth_format();

      VkExtent2D extent = swapchain.get_extent();

      VkImageCreateInfo depth_image_info{
	.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
	.imageType = VK_IMAGE_TYPE_2D,
	.format = depth_format,
	.extent = {extent.width, extent.height, 1},
	.mipLevels = 1,
	.arrayLayers = 1,
	.samples = VK_SAMPLE_COUNT_1_BIT,
	.tiling = VK_IMAGE_TILING_OPTIMAL,
	.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT
      };

      vkCreateImage(device.get_handle(), &depth_image_info, nullptr, &depth_image);

      VkMemoryRequirements depth_memory_requirements;
      vkGetImageMemoryRequirements(device.get_handle(), depth_image, &depth_memory_requirements);

      VkMemoryAllocateInfo depth_memory_info{
	.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
	.allocationSize = depth_memory_requirements.size,
	.memoryTypeIndex = device.get_memory_type(depth_memory_requirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
      };

      vkAllocateMemory(device.get_handle(), &depth_memory_info, nullptr, &depth_image_memory);
      vkBindImageMemory(device.get_handle(), depth_image, depth_image_memory, 0);

      depth_image_view = create_depth_image_view();
    }

    std::unique_ptr<core::ImageView> RenderTarget::create_depth_image_view() {
      return std::make_unique<core::ImageView>(device, depth_image, depth_format, VK_IMAGE_ASPECT_DEPTH_BIT);
    }

    RenderTarget::~RenderTarget() {
      vkFreeMemory(device.get_handle(), depth_image_memory, nullptr);
      vkDestroyImage(device.get_handle(), depth_image, nullptr);
    }

    core::ImageView &RenderTarget::get_depth_image_view() {
      return *depth_image_view;
    }
  }
}
