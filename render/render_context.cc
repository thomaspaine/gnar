// Thomas Paine 2024

#include "render_context.h"

namespace gnar {
  namespace render {
    RenderContext::RenderContext(core::Device &device,
				 core::Surface &surface,
				 platform::Window &window,
				 platform::AssetManager &asset_manager,
				 core::BufferContext &buffer_context) :
      device{device},
      surface{surface},
      window{window},
      asset_manager{asset_manager},
      buffer_context{buffer_context} {
      spdlog::info("Creating render context");
      create_function();
    }

    void RenderContext::create_function() {
      swapchain = std::make_unique<core::Swapchain>(device, surface);
      render_pass = std::make_unique<core::RenderPass>(device, swapchain->get_format());
      scene_graph = create_scene_graph();

    }

    void RenderContext::prepare() {
      render_target = create_render_target();
      command_pool = create_command_pool();

      uint32_t image_count = swapchain->get_images().size();
      for(int i = 0; i < image_count; i++) {
	image_views.push_back(std::make_unique<core::ImageView>(device, swapchain->get_images()[i], swapchain->get_format(), VK_IMAGE_ASPECT_COLOR_BIT));
	framebuffers.push_back(std::make_unique<core::Framebuffer>(device, get_swapchain(), get_image_view(i), get_render_pass(), get_render_target()));
      }

      command_buffer = create_command_buffer();
      descriptor_set = create_descriptor_set();
      pipeline = std::make_unique<core::Pipeline>(device, shader_modules, get_render_pass(), get_descriptor_set());
      sync = std::make_unique<core::Sync>(device);
    }

    void RenderContext::draw_frame() {
      //spdlog::info("Called draw_frame() in render_context");
      VkResult result;
      vkWaitForFences(device.get_handle(), 1, &sync->get_fence(), VK_TRUE, UINT64_MAX);
      vkResetFences(device.get_handle(), 1, &sync->get_fence());
      uint32_t image_index;
      vkAcquireNextImageKHR(device.get_handle(), swapchain->get_handle(), UINT64_MAX, sync->get_ready_semaphore(), VK_NULL_HANDLE, &image_index);
      vkResetCommandBuffer(command_buffer->get_handle(), 0);
      command_buffer->record(get_framebuffer(image_index), get_pipeline(), get_descriptor_set());
      VkSemaphore wait_semaphores[] = { sync->get_ready_semaphore() };
      VkSemaphore signal_semaphores[] = { sync->get_finished_semaphore() };
      VkPipelineStageFlags wait_stages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
      VkCommandBuffer command_buffer = get_command_buffer().get_handle();
      VkSubmitInfo submit_info = {
	.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
	.waitSemaphoreCount = 1,
	.pWaitSemaphores = wait_semaphores,
	.pWaitDstStageMask = wait_stages,
	.commandBufferCount = 1,
	.pCommandBuffers = &command_buffer,
	.signalSemaphoreCount = 1,
	.pSignalSemaphores = signal_semaphores
      };
      result = vkQueueSubmit(device.get_graphics_queue(), 1, &submit_info, sync->get_fence());
      if(result != VK_SUCCESS) {
	spdlog::error("Could not submit command buffer to queue");
	throw;
      }
      //spdlog::info("Submitted command buffer to queue");

      VkSwapchainKHR swapchains[] = { swapchain->get_handle() };

      VkPresentInfoKHR present_info = {
	.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
	.waitSemaphoreCount = 1,
	.pWaitSemaphores = signal_semaphores,
	.swapchainCount = 1,
	.pSwapchains = swapchains,
	.pImageIndices = &image_index
      };

      result = vkQueuePresentKHR(device.get_present_queue(), &present_info);
      if(result == VK_SUBOPTIMAL_KHR) {
	spdlog::info("Sub optiomal result in render context");
	// Todo need to clear images/framebuffers/swapchain from this class
	window.set_orientation_changed(true);
	swapchain->recreate();
      } else if(result == VK_ERROR_OUT_OF_DATE_KHR) {
	spdlog::info("Swap chain out of date");
	swapchain->recreate();
      } else if(result != VK_SUCCESS) {
	spdlog::error("Unexpected result presenting queue");
      }
    }

    void RenderContext::load_shader(const std::string &filename) {
      shader_modules.push_back(std::make_unique<core::ShaderModule>(filename, device, asset_manager));
    }

    void RenderContext::set_index_data(uint32_t uniform_buffer_index_) {
      uniform_buffer_index = uniform_buffer_index_;
    }

    void RenderContext::add_draw_buffer( uint32_t vertex_buffer_index_,
					 uint32_t index_buffer_index_,
					 uint32_t indices_count_ ) {
      core::CommandBuffer::DrawBuffer draw_buffer =
	{ vertex_buffer_index_,
	  index_buffer_index_,
	  indices_count_ };
      draw_buffers.push_back(draw_buffer);
    }

    core::Swapchain &RenderContext::get_swapchain() {
      return *swapchain;
    }

    core::ImageView &RenderContext::get_image_view(uint32_t index) {
      return *image_views[index];
    }

    core::RenderPass &RenderContext::get_render_pass() {
      return *render_pass;
    }

    core::Pipeline &RenderContext::get_pipeline() {
      return *pipeline;
    }

    core::Framebuffer &RenderContext::get_framebuffer(uint32_t index) {
      return *framebuffers[index];
    }

    core::CommandPool &RenderContext::get_command_pool() {
      return *command_pool;
    }

    core::CommandBuffer &RenderContext::get_command_buffer() {
      return *command_buffer;
    }

    core::Sync &RenderContext::get_sync() {
      return *sync;
    }

    core::DescriptorSet &RenderContext::get_descriptor_set() {
      return *descriptor_set;
    }

    RenderTarget &RenderContext::get_render_target() {
      return *render_target;
    }

    scene::SceneGraph &RenderContext::get_scene_graph() {
      return *scene_graph;
    }

    std::unique_ptr<core::CommandBuffer> RenderContext::create_command_buffer() {
      return std::make_unique<core::CommandBuffer>(device,
						   get_swapchain(),
						   get_command_pool(),
						   get_render_pass(),
						   draw_buffers,
						   buffer_context,
						   get_scene_graph());
    }

    std::unique_ptr<RenderTarget> RenderContext::create_render_target() {
      return std::make_unique<RenderTarget>(device, get_swapchain());
    }

    std::unique_ptr<scene::SceneGraph> RenderContext::create_scene_graph() {
      return std::make_unique<scene::SceneGraph>(device, asset_manager, buffer_context);
    }

    std::unique_ptr<core::CommandPool> RenderContext::create_command_pool() {
      return std::make_unique<core::CommandPool>(device);
    }

    std::unique_ptr<core::DescriptorSet> RenderContext::create_descriptor_set() {
      return std::make_unique<core::DescriptorSet>(device, buffer_context, get_scene_graph(), uniform_buffer_index);
    }
  }
}
