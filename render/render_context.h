// Thomas Paine, 2024

#pragma once

#include "core/device.h"
#include "core/surface.h"
#include "core/swapchain.h"
#include "core/image_view.h"
#include "core/render_pass.h"
#include "core/pipeline.h"
#include "core/shader_module.h"
#include "core/framebuffer.h"
#include "core/command_pool.h"
#include "core/command_buffer.h"
#include "core/sync.h"
#include "core/buffer_context.h"
#include "core/buffer.h"

#include "render/render_target.h"

#include "scene/scene_graph.h"

#include "platform/asset_manager_delegator.h"

namespace gnar {
  namespace render {
    class RenderContext {
    public:
      RenderContext(core::Device &device,
		    core::Surface &surface,
		    platform::Window &window,
		    platform::AssetManager &asset_manager,
		    core::BufferContext &buffer_context);
      void prepare();
      void draw_frame();

      void load_shader(const std::string &filename);

      void set_index_data(uint32_t uniform_buffer_index_);

      void add_draw_buffer
      ( uint32_t vertex_buffer_index_,
	uint32_t index_buffer_index_,
	uint32_t indices_count_
      );

      core::Swapchain &get_swapchain();
      core::ImageView &get_image_view(uint32_t index);
      core::RenderPass &get_render_pass();
      core::Pipeline &get_pipeline();
      core::Framebuffer &get_framebuffer(uint32_t index);
      core::CommandPool &get_command_pool();
      core::CommandBuffer &get_command_buffer();
      core::Sync &get_sync();
      core::DescriptorSet &get_descriptor_set();
      RenderTarget &get_render_target();
      scene::SceneGraph &get_scene_graph();

    private:
      core::Device &device;
      core::Surface &surface;
      platform::Window &window;
      platform::AssetManager &asset_manager;
      core::BufferContext &buffer_context;

      void create_function();
      void recreate();

      std::unique_ptr<core::Swapchain> swapchain;
      std::vector<std::unique_ptr<core::ImageView>> image_views;
      std::unique_ptr<core::RenderPass> render_pass;
      std::vector<std::unique_ptr<core::ShaderModule>> shader_modules;
      std::unique_ptr<core::Pipeline> pipeline;
      std::vector<std::unique_ptr<core::Framebuffer>> framebuffers;
      std::unique_ptr<core::CommandPool> command_pool;
      std::unique_ptr<core::CommandBuffer> command_buffer;
      std::unique_ptr<core::Sync> sync;
      std::unique_ptr<core::DescriptorSet> descriptor_set;
      std::unique_ptr<RenderTarget> render_target;
      std::unique_ptr<scene::SceneGraph> scene_graph;

      std::unique_ptr<RenderTarget> create_render_target();
      std::unique_ptr<scene::SceneGraph> create_scene_graph();
      std::unique_ptr<core::CommandPool> create_command_pool();
      std::unique_ptr<core::DescriptorSet> create_descriptor_set();
      std::unique_ptr<core::CommandBuffer> create_command_buffer();

      std::vector<core::CommandBuffer::DrawBuffer> draw_buffers{};

      uint32_t indices_count;
      uint32_t index_buffer_index;
      uint32_t vertex_buffer_index;
      uint32_t uniform_buffer_index;
    };
  }
}
