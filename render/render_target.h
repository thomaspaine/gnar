
#pragma once

#include "common/includes.h"

#include "core/device.h"
#include "core/swapchain.h"
#include "core/image_view.h"

namespace gnar {
  namespace render {
    class RenderTarget {
    public:
      RenderTarget(core::Device &device, core::Swapchain &swapchain);
      ~RenderTarget();

      core::ImageView &get_depth_image_view();
    private:
      core::Device &device;
      core::Swapchain &swapchain;

      VkImage depth_image;
      VkDeviceMemory depth_image_memory;
      VkFormat depth_format;
      std::unique_ptr<core::ImageView> depth_image_view;
      std::unique_ptr<core::ImageView> create_depth_image_view();

      VkImage alias_image;
      VkDeviceMemory alias_image_memory;
      VkFormat alias_format;
      std::unique_ptr<core::ImageView> alias_image_view;
      std::unique_ptr<core::ImageView> create_alias_image_view();
    };
  }
}
