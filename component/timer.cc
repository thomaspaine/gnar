// Thomas Paine, 2024
// Timer

#include "timer.h"

namespace gnar {
  namespace component {
    Timer::Timer() {
      spdlog::info("Creating timer");
    }
    void Timer::set_interval(uint32_t interval_) {
      //interval = interval_
    }
    void Timer::start() {
      spdlog::info("Starting timer");
      last_tick = Clock::now();
    }
    double Timer::tick() {
      Clock::time_point now = Clock::now();
      std::chrono::duration duration = std::chrono::duration<double, std::ratio<30,1>>(now - last_tick);
      last_tick = now;
      double count = duration.count();
      delta_time += count;
      if(delta_time > 1.0) {
	seconds++;
	//spdlog::info("{}x10 seconds", seconds);
	delta_time = 0.0;
      }
      //spdlog::info("fps: {}", 1/duration.count());
      return duration.count();
    }
    double Timer::get_delta_time() {
      return delta_time;
    }
  }
}
