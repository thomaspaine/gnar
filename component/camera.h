// Thomas Paine, 2024
// Camera header

#pragma once

#include "common/includes.h"

#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>

namespace gnar {
  namespace component {
    class Camera {
    public:
      enum CameraType { LookAt, FirstPerson, Sensor };

      Camera();

      void update();
      void set_position(glm::vec3);
      void set_rotation(glm::quat);
      void set_perspective(float, float, float, float);
      void set_view(glm::vec3, glm::vec3);

      glm::mat4 get_perspective();
      glm::mat4 get_view();

      void pan_left();
      void pan_right();
      void pan_up();
      void pan_down();

      void move_left();
      void move_right();
      void move_forward();
      void move_back();
private:
      glm::vec3 position;
      glm::quat rotation;
      glm::mat4 perspective;
      glm::mat4 view;
      CameraType type;
    };
  }
}
