// Thomas Paine 2024

#include "camera.h"

namespace gnar {
  namespace component {
    Camera::Camera() {
      position = glm::vec3(0.0f, 0.0f, 0.0f);
      rotation = glm::quat(1.0f, glm::vec3(0.0f, 0.0f, 0.0f));
    }

    glm::mat4 Camera::get_view() {
      return view;
    }

    glm::mat4 Camera::get_perspective() {
      return perspective;
    }

    void Camera::update() {
#ifdef ANDROID
      return; // TODO remove
#endif
      glm::vec3 focus = glm::cross(rotation, glm::vec3(0.0f, 0.0f, 1.0f));
      set_view(position, position + focus);
    }

    void Camera::set_position(glm::vec3 position_) {
      position = position_;
    }

    // Android TODO 90 degree rotation handled by surface VkSurfaceTransformFlagBitsKHR
    void Camera::set_rotation(glm::quat rotation_) {
      view = glm::mat4_cast(glm::rotate(rotation_, 3.14f/2.f, glm::vec3(1, 0, 0)));
    }

    void Camera::pan_left() {
      glm::quat axis_rotation = glm::angleAxis(0.01f, glm::vec3(0.f, -1.f, 0.f));
      rotation *= axis_rotation;
    }
    void Camera::pan_right() {
      glm::quat axis_rotation = glm::angleAxis(-0.01f, glm::vec3(0.f, -1.f, 0.f));
      rotation *= axis_rotation;
    }
    void Camera::pan_up() {
      glm::quat axis_rotation = glm::angleAxis(-0.01f, glm::vec3(1.f, 0.f, 0.f));
      rotation *= axis_rotation;
    }
    void Camera::pan_down() {
      glm::quat axis_rotation = glm::angleAxis(0.01f, glm::vec3(1.f, 0.f, 0.f));
      rotation *= axis_rotation;
    }

    void Camera::move_left() {
      glm::vec3 focus = glm::rotate(rotation, glm::vec3(0.0f, 0.0f, 1.0f));
      focus = glm::cross(focus, glm::vec3(0.f, -1.f, 0.f));
      position -= 0.05f*focus;
    }
    void Camera::move_right() {
      glm::vec3 focus = glm::rotate(rotation, glm::vec3(0.0f, 0.0f, 1.0f));
      focus = glm::cross(focus, glm::vec3(0.f, -1.f, 0.f));
      position += 0.05f*focus;
    }
    void Camera::move_forward() {
      glm::vec3 focus = glm::rotate(rotation, glm::vec3(0.0f, 0.0f, 1.0f));
      position += 0.05f*focus;
    }
    void Camera::move_back() {
      glm::vec3 focus = glm::rotate(rotation, glm::vec3(0.0f, 0.0f, 1.0f));
      position -= 0.05f*focus;
    }

    void Camera::set_perspective(float fov, float aspect, float near, float far) {
      perspective = glm::perspective(fov, aspect, near, far);
    }

    void Camera::set_view(glm::vec3 eye, glm::vec3 focus) {
      glm::vec3 up = glm::vec3(0.f,-1.f,0.f);

      view = glm::lookAt
	( eye,
	  focus,
	  up
	  );

    }
  }
}
