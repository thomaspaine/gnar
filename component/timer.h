// Thomas Paine, 2024
// Timer header

#pragma once

#include <chrono>

#include "common/includes.h"

using Clock = std::chrono::steady_clock;

namespace gnar {
  namespace component {
    class Timer {
    public:
      Timer();
      void start();
      double tick();
      double get_delta_time();
      void set_interval(uint32_t interval_);
    private:
      Clock::time_point last_tick;
      double delta_time = 0.0;
      uint32_t seconds = 0;
      uint32_t interval;
    };
  }
}
