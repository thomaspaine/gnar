// Thomas Paine, 2024

#include "scene_graph.h"

namespace gnar {
  namespace scene {
    SceneGraph::SceneGraph(core::Device &device,
			   platform::AssetManager &asset_manager,
			   core::BufferContext &buffer_context) :
      device{device},
      asset_manager{asset_manager},
      buffer_context{buffer_context}
    {
      spdlog::info("Creating scene graph");
    }

    uint32_t SceneGraph::get_texture_count() {
      return textures.size();
    }

    Texture &SceneGraph::get_texture(uint32_t index) {
      return *textures[index];
    }

    Model &SceneGraph::get_model(uint32_t index) {
      return *models[index];
    }

    uint32_t SceneGraph::load_texture(const std::string &filename) {
      textures.push_back(std::make_unique<Texture>(filename, device, asset_manager, buffer_context));
      return textures.size() -1;
    }

    uint32_t SceneGraph::load_model(const std::string &filename) {
      models.push_back(std::make_unique<Model>(filename, asset_manager));
      return models.size() -1;
    }

    uint32_t SceneGraph::prepare_model(uint32_t model_index) {
      Model &model = get_model(model_index);
      std::vector<Vertex> &vertices = model.get_vertices();
      std::vector<uint16_t> &indices = model.get_indices();
      uint32_t vertex_buffer_index = buffer_context.build_buffer(vertices.size()*sizeof(Vertex), VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
      gnar::core::Buffer &vertex_buffer = buffer_context.get_buffer(vertex_buffer_index);
      uint32_t index_buffer_index = buffer_context.build_buffer(indices.size()*sizeof(uint32_t), VK_BUFFER_USAGE_INDEX_BUFFER_BIT);
      gnar::core::Buffer &index_buffer = buffer_context.get_buffer(index_buffer_index);
      vertex_buffer.map_memory(vertices.data(), vertices.size()*sizeof(Vertex));
      index_buffer.map_memory(indices.data(), indices.size()*sizeof(uint32_t));
      return model_index;
    }
  }
}
