// Thomas Paine, 2024

#include "model.h"

namespace gnar {
  namespace scene {
    Model::Model(const std::string filename, platform::AssetManager &asset_manager) {
      spdlog::info("Loading model: '{}'", filename);

      tinygltf::TinyGLTF loader;
      tinygltf::Model model;
      std::string err;
      std::string warn;

      std::vector<char> data = asset_manager.read_file("models/" + filename);

      bool result = loader.LoadBinaryFromMemory
	(&model, &err, &warn,
	 reinterpret_cast<unsigned char*>(data.data()),
	 reinterpret_cast<size_t>(data.size()),
	 "models/"
	 );
      //bool result = loader.LoadASCIIFromFile(&model, &err, &warn, "models/" + filename);

      spdlog::error("Error: {}", err);
      spdlog::warn("Warning: {}", warn);

      const float *coords = nullptr;
      const float *normals = nullptr;
      const float *uvs = nullptr;
      const uint16_t *indexs = nullptr;

      for(tinygltf::Mesh mesh : model.meshes) {
	tinygltf::Primitive &primitive = mesh.primitives[0];
	tinygltf::Accessor &accessor = model.accessors[primitive.attributes.find("POSITION")->second];
	tinygltf::BufferView &buffer_view = model.bufferViews[accessor.bufferView];
	tinygltf::Buffer &buffer = model.buffers[buffer_view.buffer];
	coords = reinterpret_cast<const float *>(&(buffer.data[accessor.byteOffset + buffer_view.byteOffset]));
	size_t vertex_count = accessor.count;

	accessor = model.accessors[primitive.attributes.find("NORMAL")->second];
	buffer_view = model.bufferViews[accessor.bufferView];
	buffer = model.buffers[buffer_view.buffer];
	normals = reinterpret_cast<const float *>(&(buffer.data[accessor.byteOffset + buffer_view.byteOffset]));

	accessor = model.accessors[primitive.attributes.find("TEXCOORD_0")->second];
	buffer_view = model.bufferViews[accessor.bufferView];
	buffer = model.buffers[buffer_view.buffer];
	uvs = reinterpret_cast<const float *>(&(buffer.data[accessor.byteOffset + buffer_view.byteOffset]));

	for(size_t i = 0; i < vertex_count; i++) {
	  Vertex vertex{};
	  vertex.coord = glm::make_vec3(&coords[i * 3]);
	  vertex.normal = glm::make_vec3(&normals[i * 3]);
	  vertex.uv = glm::make_vec3(&uvs[i * 2]);
	  vertices.push_back(vertex);
	  //spdlog::info("Normal: x: {}, y: {}, z: {}", vertex.normal.x, vertex.normal.y, vertex.normal.z);
	}
	spdlog::info("Mesh vertexes: {}", accessor.count);

	accessor = model.accessors[primitive.indices];
	buffer_view = model.bufferViews[accessor.bufferView];
	buffer = model.buffers[buffer_view.buffer];
	indexs = reinterpret_cast<const uint16_t *>(&(buffer.data[accessor.byteOffset + buffer_view.byteOffset]));

	for(size_t i = 0; i < accessor.count; i++) {
	  uint16_t index = indexs[i];
	  indices.push_back(index);
	}
	spdlog::info("Mesh indices: {}", accessor.count);
      }
    }

    std::vector<Vertex> &Model::get_vertices() {
      return vertices;
    }

    std::vector<uint16_t> &Model::get_indices() {
      return indices;
    }
  }
}
