// Thomas Paine, 2024

#include "texture.h"

namespace gnar {
  namespace scene {
    Texture::Texture(const std::string filename,
		     core::Device &device,
		     platform::AssetManager &asset_manager,
		     core::BufferContext &buffer_context) :
      device{device},
      format{VK_FORMAT_R8G8B8A8_SRGB} {
      spdlog::info("Creating texture: '{}'", filename);
      VkResult result;
      KTX_error_code ktx_result;
      ktxTexture *texture;
      ktxVulkanDeviceInfo ktx_device;

      std::vector<char> data = asset_manager.read_file("textures/" + filename);
      ktx_result = ktxTexture_CreateFromMemory(reinterpret_cast<ktx_uint8_t*>(data.data()),
					      reinterpret_cast<ktx_size_t>(data.size()),
					      KTX_TEXTURE_CREATE_NO_FLAGS,
					      &texture);
      if(ktx_result != KTX_SUCCESS) {
	spdlog::error("Could not create texture from memory");
	throw;
      }

      std::vector<ktx_uint8_t> texture_data(texture->dataSize);
      ktx_result = ktxTexture_LoadImageData(texture, texture_data.data(), texture->dataSize);
      if(ktx_result != KTX_SUCCESS) {
	spdlog::error("Could not load texture data");
	throw;
      }

      staging_index = buffer_context.build_buffer(texture->dataSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT);
      core::Buffer &staging_buffer = buffer_context.get_buffer(staging_index);
      staging_buffer.map_memory(texture_data.data(), texture->dataSize);

      spdlog::info("Loaded texture data");

      width = texture->baseWidth;
      height = texture->baseHeight;

      VkImageCreateInfo image_info = {
	.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
	.imageType = VK_IMAGE_TYPE_2D,
	.format = format,
	.extent = {
	  .width = width,
	  .height = height,
	  .depth = 1
	},
	.mipLevels = 1,
	.arrayLayers = 1,
	.samples = VK_SAMPLE_COUNT_1_BIT,
	.tiling = VK_IMAGE_TILING_OPTIMAL,
	.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
	.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
	.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
      };

      result = vkCreateImage(device.get_handle(), &image_info, nullptr, &image);

      if(result != VK_SUCCESS) {
	spdlog::error("Could not create texture image");
	throw;
      }
      spdlog::info("Created texture image");

      VkMemoryRequirements memory_requirements;
      vkGetImageMemoryRequirements(device.get_handle(), image, &memory_requirements);

      // TODO Memory allocate to seperate class
      VkMemoryAllocateInfo image_memory_info = {
	.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
	.allocationSize = memory_requirements.size,
	.memoryTypeIndex = device.get_memory_type(memory_requirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
      };

	result = vkAllocateMemory(device.get_handle(), &image_memory_info, nullptr, &image_memory);
	if(result != VK_SUCCESS) {
	  spdlog::error("Could not allocate texture memory");
	  throw;
	}

	vkBindImageMemory(device.get_handle(), image, image_memory, 0);

	image_view = create_image_view();

	VkSamplerCreateInfo sampler_info{
	  .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
	  .magFilter = VK_FILTER_LINEAR,
	  .minFilter = VK_FILTER_LINEAR,
	};

	vkCreateSampler(device.get_handle(), &sampler_info, nullptr, &sampler);

	image_descriptor = {
	  .sampler = get_sampler(),
	  .imageView = get_image_view().get_handle(),
	  .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
	};

    }

    Texture::~Texture() {
      spdlog::info("Destroying texture");
      vkDestroyImage(device.get_handle(), image, nullptr);
      vkDestroySampler(device.get_handle(), sampler, nullptr);
      vkFreeMemory(device.get_handle(), image_memory, nullptr);
    }

    core::ImageView &Texture::get_image_view() {
      return *image_view;
    }

    VkDescriptorImageInfo &Texture::get_image_descriptor() {
      return image_descriptor;
    }

    VkSampler &Texture::get_sampler() {
      return sampler;
    }

    VkImage &Texture::get_image() {
      return image;
    }

    uint32_t &Texture::get_staging_index() {
      return staging_index;
    }

    uint32_t &Texture::get_width() {
      return width;
    }

    uint32_t &Texture::get_height() {
      return height;
    }

    std::unique_ptr<core::ImageView> Texture::create_image_view() {
      return std::make_unique<core::ImageView>(device, image, format, VK_IMAGE_ASPECT_COLOR_BIT);
    }
  }
}
