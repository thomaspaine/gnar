// Thomas Paine, 2024

#pragma once

#include "common/includes.h"

#include "core/device.h"
#include "platform/asset_manager_delegator.h"
#include "core/command_pool.h"
#include "core/buffer_context.h"

#include "scene/texture.h"
#include "scene/model.h"

namespace gnar {
  namespace scene {
    class SceneGraph {
    public:
      SceneGraph(core::Device &device,
		 platform::AssetManager &asset_manager,
		 core::BufferContext &buffer_context);

      uint32_t load_texture(const std::string &filename);
      uint32_t load_model(const std::string &filename);

      uint32_t get_texture_count();

      Texture &get_texture(uint32_t);
      Model &get_model(uint32_t);
    private:
      core::Device &device;
      platform::AssetManager &asset_manager;
      core::BufferContext &buffer_context;

      std::vector<std::unique_ptr<Texture>> textures;
      std::vector<std::unique_ptr<Model>> models;

      uint32_t prepare_model(uint32_t model_index);
    };
  }
}
