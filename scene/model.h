// Thomas Paine, 2024

#pragma once

#include "common/includes.h"

#include "platform/asset_manager_delegator.h"

#include <tiny_gltf.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace gnar {
  namespace scene {
    struct Vertex {
      glm::vec3 coord;
      glm::vec3 normal;
      glm::vec2 uv;
    };

    class Model {
    public:
      Model(const std::string filename, platform::AssetManager &asset_manager);

      std::vector<uint16_t> &get_indices();
      std::vector<Vertex> &get_vertices();
    private:
      std::vector<uint16_t> indices;
      std::vector<Vertex> vertices;
    };
  }
}
