// Thomas Paine, 2024

#pragma once

#include "common/includes.h"

#include "core/device.h"
#include "platform/asset_manager_delegator.h"
#include "core/command_pool.h"
#include "core/buffer_context.h"
#include "core/image_view.h"

#include <ktxvulkan.h>

namespace gnar {
  namespace scene {
    class Texture {
    public:
      Texture(const std::string filename,
	      core::Device &device,
	      platform::AssetManager &asset_manager,
	      core::BufferContext &buffer_context);
      ~Texture();

      core::ImageView &get_image_view();
      VkSampler &get_sampler();
      VkImage &get_image();
      VkDescriptorImageInfo &get_image_descriptor();

      uint32_t &get_staging_index();

      uint32_t &get_width();
      uint32_t &get_height();
    private:
      core::Device &device;

      VkFormat format;

      std::unique_ptr<core::ImageView> image_view;
      std::unique_ptr<core::ImageView> create_image_view();

      VkImage image;
      VkDeviceMemory image_memory;
      VkSampler sampler;

      VkDescriptorImageInfo image_descriptor;

      uint32_t staging_index;

      uint32_t width;
      uint32_t height;
    };
  }
}
