// Thomas Paine, 2024

#include "Gnar.h"

Gnar::Gnar() {
  prepared = false;
  destroy_requested = false;
}

void Gnar::prepare() {
  spdlog::info("Prepare called in engine");
  volkInitialize();
  window = create_window();
  asset_manager = create_asset_manager();
  instance = create_instance();
  surface = create_surface();
  physical_device = create_physical_device();
  device = create_device();
  buffer_context = create_buffer_context();
  render_context = create_render_context();
  camera = create_camera();
  timer = create_timer();
}

void Gnar::update() {
  destroy_requested = window->should_close();
  window->process_events();
  render_context->draw_frame();
  timer->tick();
  //spdlog::info("Update called in engine");
}

void Gnar::cleanup() {
  vkDeviceWaitIdle(device->get_handle());
}

gnar::platform::Window &Gnar::get_window() {
  return *window;
}

gnar::core::Instance &Gnar::get_instance() {
  return *instance;
}

gnar::core::PhysicalDevice &Gnar::get_physical_device() {
  return *physical_device;
}

gnar::core::Device &Gnar::get_device() {
  return *device;
}

gnar::core::Surface &Gnar::get_surface() {
  return *surface;
}

gnar::render::RenderContext &Gnar::get_render_context() {
  return *render_context;
}

gnar::component::Camera &Gnar::get_camera() {
  return *camera;
}

gnar::component::Timer &Gnar::get_timer() {
  return *timer;
}

gnar::platform::AssetManager &Gnar::get_asset_manager() {
  return *asset_manager;
}

// gnar::core::UniformBuffer &Gnar::get_uniform_buffer() {
//   return *uniform_buffer;
// }
gnar::core::BufferContext &Gnar::get_buffer_context() {
  return *buffer_context;
}

std::unique_ptr<gnar::platform::Window> Gnar::create_window() {
  return std::make_unique<gnar::platform::Window>(get_platform(), get_application_name());
}

std::unique_ptr<gnar::core::Instance> Gnar::create_instance() {
  return std::make_unique<gnar::core::Instance>(get_platform(), get_application_name(), get_engine_name());
}

std::unique_ptr<gnar::core::PhysicalDevice> Gnar::create_physical_device() {
  return std::make_unique<gnar::core::PhysicalDevice>(get_instance(), get_surface());
}

std::unique_ptr<gnar::core::Device> Gnar::create_device() {
  return std::make_unique<gnar::core::Device>(get_physical_device());
}

std::unique_ptr<gnar::core::Surface> Gnar::create_surface() {
  return std::make_unique<gnar::core::Surface>(get_instance(), get_window());
}

std::unique_ptr<gnar::render::RenderContext> Gnar::create_render_context() {
  return std::make_unique<gnar::render::RenderContext>(get_device(), get_surface(), get_window(), get_asset_manager(), get_buffer_context());
}

std::unique_ptr<gnar::component::Camera> Gnar::create_camera() {
  return std::make_unique<gnar::component::Camera>();
}

std::unique_ptr<gnar::component::Timer> Gnar::create_timer() {
  return std::make_unique<gnar::component::Timer>();
}

std::unique_ptr<gnar::platform::AssetManager> Gnar::create_asset_manager() {
  return std::make_unique<gnar::platform::AssetManager>(get_platform());
}

std::unique_ptr<gnar::core::BufferContext> Gnar::create_buffer_context() {
  return std::make_unique<gnar::core::BufferContext>(get_device());
}
