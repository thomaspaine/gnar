// Thomas Paine, 2024
// Filesystem support

#include "asset_manager_android.h"

#include <string>

namespace gnar {
  namespace platform {
    AssetManager::AssetManager(const Platform &platform) :
      handle{platform.asset_manager} {
      spdlog::info("Creating Asset Manager");
    }
    std::vector<char> AssetManager::read_file(const std::string &filename) {
      std::string filepath = filename;

      spdlog::info("Reading file {}", filename);
      AAsset *file = AAssetManager_open(handle, filepath.c_str(), AASSET_MODE_BUFFER);
      size_t filesize = AAsset_getLength(file);
      spdlog::info("File size: {}", filesize);
      std::vector<char> buffer(filesize);
      AAsset_read(file, buffer.data(), filesize);
      AAsset_close(file);
      return buffer;
    }

    // ktxTexture AssetManager::read_texture(const std::string &filename) {

    // }
  }
}
