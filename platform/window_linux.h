// Thomas Paine, 2024

#pragma once

//#include "common/includes.h"
#include <GLFW/glfw3.h>

#include "platform/application.h"
#include "platform/input_event.h"

namespace gnar {
  namespace platform {
    class Window {
    public:
      Window(const Platform &platform, std::string title);
      ~Window();
      bool should_close();
      void set_should_close();

      void process_events();
      void set_orientation_changed(bool orientation_changed_);

      GLFWwindow &get_handle();
      //private:
      GLFWwindow *handle;
    private:
      bool orientation_changed;
    };
  }
}
