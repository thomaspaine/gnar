// Thomas Paine, 2024
// Filesystem support

#pragma once

//#include "common/includes.h"
#ifndef ANDROID
#include <fstream>
#include <filesystem>
#endif

#include "platform/platform_delegator.h"

namespace gnar {
  namespace platform {
    class AssetManager {
    public:
      AssetManager(const Platform &platform);
      std::vector<char> read_file(const std::string &filename);
    private:
      std::string shader_path = "shaders";
    };
  }
}
