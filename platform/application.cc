// Thomas Paine, 2024

#include "application.h"

namespace gnar {
  namespace platform {
    Application::Application() : application_name{"Vulkan Luvios"} {
    }
#ifdef ANDROID
    void Application::create_platform(Platform::PlatformOptions platform_options, ANativeWindow *&window, AAssetManager *&asset_manager, android_app *app) {
      platform = std::make_unique<Platform>(platform_options, window, asset_manager, app);
    }
#else
    void Application::create_platform(Platform::PlatformOptions platform_options) {
      platform = std::make_unique<Platform>(platform_options);
    }
#endif
    Platform &Application::get_platform() {
      return *platform;
    }
    const std::string &Application::get_application_name() const {
      return application_name;
    }
    const std::string &Application::get_engine_name() const {
      return engine_name;
    }
    void Application::set_application_name(const std::string &application_name_) {
      application_name = application_name_;
    }
    void Application::set_engine_name(const std::string &engine_name_) {
      engine_name = engine_name_;
    }
  }
}
