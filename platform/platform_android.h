// Thomas Paine, 2024

#pragma once

#include <game-activity/native_app_glue/android_native_app_glue.h>

#include "common/includes.h"

#include "platform/input_event.h"
#include "platform/sensor.h"


namespace gnar {
  namespace platform {
    class Platform {
    public:
      struct PlatformOptions {
	std::vector<const char*> extensions;
	std::vector<const char*> layers;
      };
      // Todo remove window and asset manager from constructor, pull from app
      Platform(PlatformOptions platform_options, ANativeWindow *&window, AAssetManager *&asset_manager, android_app *app);

      void update();

      const std::vector<const char*> &get_required_extensions() const;
      const std::vector<const char*> &get_required_layers() const;


      void start_sensors();
      void stop_sensors();

      //private:
      AAssetManager *&asset_manager;
      ANativeWindow *&window;
      void input_event(InputEvent event);
      const std::vector<InputEvent> &get_input_events() const;
      const void clear_input_events();

      AAssetManager *get_asset_manager() const;

      android_app *get_app();
    private:
      android_app *app;

      std::unique_ptr<Sensor> sensor;

      std::unique_ptr<Sensor> create_sensor();

      std::vector<InputEvent> input_events;

      std::vector<const char*> extensions;
      std::vector<const char*> layers;
    };
  }
}
