// Thomas Paine, 2024

#pragma once

#include "common/includes.h"

#include "platform/platform_delegator.h"

namespace gnar {
  namespace platform {
    class Application {
    public:
      Application();

      const std::string &get_application_name() const;
      const std::string &get_engine_name() const;
      Platform &get_platform();

      void set_application_name(const std::string &application_name);
      void set_engine_name(const std::string &engine_name);

#ifdef ANDROID
      void create_platform(Platform::PlatformOptions platform_options, ANativeWindow *&window, AAssetManager *&asset_manager, android_app *app);
#else
      void create_platform(Platform::PlatformOptions platform_options);
#endif
    private:
      std::string application_name;
      std::string engine_name = "Luvios";
      std::unique_ptr<Platform> platform;
    };
  }
}
