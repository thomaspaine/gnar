// Thomas Paine, 2024

#include "window_linux.h"

namespace gnar {
  namespace platform {
    void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
      auto *platform = reinterpret_cast<gnar::platform::Platform *>(glfwGetWindowUserPointer(window));
      if(yoffset == 1 || yoffset == -1) {
	bool scroll_up = yoffset == 1;
	platform->input_event(MouseScrollEvent(scroll_up));
      }
    }

    void cursor_position_callback(GLFWwindow* window, double xoffset, double yoffset) {
      auto *platform = reinterpret_cast<gnar::platform::Platform *>(glfwGetWindowUserPointer(window));
      platform->input_event(MouseMoveEvent(xoffset, yoffset));
    }

    void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
      auto *platform = reinterpret_cast<gnar::platform::Platform *>(glfwGetWindowUserPointer(window));
      // Arrow keys
      if (key == GLFW_KEY_LEFT || key == GLFW_KEY_A) {
	if(action == GLFW_REPEAT) {
	  platform->input_event(KeyLeftPressEvent());
	} else if(action == GLFW_RELEASE) {
	  platform->input_event(KeyLeftReleaseEvent());
	}
      }
      if (key == GLFW_KEY_RIGHT || key == GLFW_KEY_D) {
	if(action == GLFW_REPEAT) {
	  platform->input_event(KeyRightPressEvent());
	} else if(action == GLFW_RELEASE) {
	  platform->input_event(KeyRightReleaseEvent());
	}
      }
      if (key == GLFW_KEY_UP || key == GLFW_KEY_W) {
	if(action == GLFW_REPEAT) {
	  platform->input_event(KeyUpPressEvent());
	} else if(action == GLFW_RELEASE) {
	  platform->input_event(KeyUpReleaseEvent());
	}
      }
      if (key == GLFW_KEY_DOWN || key == GLFW_KEY_S) {
	if(action == GLFW_REPEAT) {
	  platform->input_event(KeyDownPressEvent());
	} else if(action == GLFW_RELEASE) {
	  platform->input_event(KeyDownReleaseEvent());
	}
      }

      // Escape
      if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
	platform->input_event(KeyEscPressEvent());
      }
    }

    Window::Window(const Platform &platform, std::string title) {
      spdlog::info("Creating GLFW window");

      glfwInit();
      glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
      glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
      handle = glfwCreateWindow(platform.get_width(), platform.get_height(), title.c_str(), nullptr, nullptr);
      glfwSetWindowUserPointer(handle, (void *) &platform);
      glfwSetScrollCallback(handle, scroll_callback);
      glfwSetCursorPosCallback(handle, cursor_position_callback);
      glfwSetKeyCallback(handle, key_callback);
      glfwSetInputMode(handle, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
      spdlog::info("Created GLFW window");

      orientation_changed = false;
    }

    Window::~Window() {
      spdlog::info("Terminate window");
      glfwDestroyWindow(handle);
      glfwTerminate();
    }

    bool Window::should_close() {
      return glfwWindowShouldClose(handle);
    }

    void Window::set_should_close() {
      glfwSetWindowShouldClose(handle, true);
    }

    void Window::process_events() {
      glfwPollEvents();
    }

    GLFWwindow &Window::get_handle() {
      return *handle;
    }

    void Window::set_orientation_changed(bool orientation_changed_) {
      orientation_changed = orientation_changed_;
    }
  }
}
