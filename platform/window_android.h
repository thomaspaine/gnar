// Thomas Paine, 2024

#pragma once

//#include "common/helpers.h"

#include "platform/application.h"

namespace gnar {
  namespace platform {
    class Window {
    public:
      Window(Platform &platform, std::string title);
      bool should_close();
      void process_events();

      void set_orientation_changed(bool orientation_changed_);
      void set_should_close();

      ANativeWindow &get_handle();
    private:
      Platform &platform;

      ANativeWindow *handle;
      bool android_window_should_close;
      bool orientation_changed;

      uint32_t width;
      uint32_t height;

      float last_distance;

      bool input_dragging;
      glm::vec2 input_drag_start;
      glm::vec2 input_drag_last_cursor;
    };
  }
}
