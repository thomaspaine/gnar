// Thomas Paine, 2024

#include "sensor.h"

namespace gnar {
  namespace platform {
    Sensor::Sensor() {
      spdlog::info("Creating sensor");
      sensorManager = ASensorManager_getInstanceForPackage("com.tjjpaine.acropolis");
      assert(sensorManager != NULL);

      rotationVectorSensor = ASensorManager_getDefaultSensor(sensorManager, ASENSOR_TYPE_GAME_ROTATION_VECTOR);
      assert(rotationVectorSensor != NULL);

      looper = ALooper_prepare(ALOOPER_PREPARE_ALLOW_NON_CALLBACKS);
      assert(looper != NULL);

      sensorEventQueue = ASensorManager_createEventQueue(sensorManager, looper,
						     LOOPER_ID_USER, NULL, NULL);
      assert(sensorEventQueue != NULL);

      auto status = ASensorEventQueue_enableSensor(sensorEventQueue, rotationVectorSensor);
      assert(status >= 0);
      status = ASensorEventQueue_setEventRate(sensorEventQueue,
					      rotationVectorSensor,
					      SENSOR_REFRESH_PERIOD_US);
      assert(status >= 0);

    }

    std::vector<InputEvent> Sensor::update() {
      ALooper_pollAll(0, NULL, NULL, NULL);
      ASensorEvent event;

      float azimuth = 0.f;
      float pitch = 0.f;
      float roll = 0.f;
      while (ASensorEventQueue_getEvents(sensorEventQueue, &event, 1) > 0) {
	if(event.vector.status != ASENSOR_STATUS_UNRELIABLE) {
	  azimuth = event.vector.azimuth;
	  pitch = -event.vector.pitch;
	  roll = event.vector.roll;
	}
      }

      x = azimuth;
      y = pitch;
      z = roll;

      w = 1 - x*x -  y*y - z*z;
      w = w > 0 ? sqrt(w) : 0;


      std::vector<InputEvent> input_events;
      //spdlog::info("quat; w: {}, x: {}, y: {}: z: {}", w, azimuth, pitch, roll);
      if(w != 1.0f) {
	glm::quat rotation(w, glm::vec3(azimuth, pitch, roll));
	input_events.push_back(RotationSensorEvent(rotation));
      }
      // if(x > 10.f) {
      // 	input_events.push_back(ShakeRightEvent());
      // } else if(x < -10.f) {
      // 	input_events.push_back(ShakeLeftEvent());
      // }
      return input_events;
    }
  }
}
