// Thomas Paine 2024
// Input event wrapper
// TODO keyboard event should carry key


#include "input_event.h"

namespace gnar {
  namespace platform {
    MouseScrollEvent::MouseScrollEvent(bool scroll_up) {
      InputEvent::action = Mouse_Scroll;
      InputEvent::scroll_up = scroll_up;
    }
    MouseMoveEvent::MouseMoveEvent(float x, float y) {
      InputEvent::action = Mouse_Move;
      InputEvent::cursor_position = {x, y};
    }

    // Keyboard
    KeyLeftPressEvent::KeyLeftPressEvent() {
      InputEvent::action = Key_Left;
    }
    KeyRightPressEvent::KeyRightPressEvent() {
      InputEvent::action = Key_Right;
    }
    KeyUpPressEvent::KeyUpPressEvent() {
      InputEvent::action = Key_Up;
    }
    KeyDownPressEvent::KeyDownPressEvent() {
      InputEvent::action = Key_Down;
    }

    KeyLeftReleaseEvent::KeyLeftReleaseEvent() {
      InputEvent::action = Release_Left;
    }
    KeyRightReleaseEvent::KeyRightReleaseEvent() {
      InputEvent::action = Release_Right;
    }
    KeyUpReleaseEvent::KeyUpReleaseEvent() {
      InputEvent::action = Release_Up;
    }
    KeyDownReleaseEvent::KeyDownReleaseEvent() {
      InputEvent::action = Release_Down;
    }

    KeyEscPressEvent::KeyEscPressEvent() {
      InputEvent::action = Key_Esc;
    }

    // Drag
    DragBeginEvent::DragBeginEvent(glm::vec2 cursor_position_) {
      InputEvent::action = Drag_Begin;
      InputEvent::cursor_position = cursor_position_;
    }
    DragMoveEvent::DragMoveEvent(glm::vec2 cursor_position_) {
      InputEvent::action = Drag_Move;
      InputEvent::cursor_position = cursor_position_;
    }
    DragEndEvent::DragEndEvent(glm::vec2 cursor_position_) {
      InputEvent::action = Drag_End;
      InputEvent::cursor_position = cursor_position_;
    }

    // Pinch
    PinchMoveEvent::PinchMoveEvent(glm::vec2 cursor_1_position, glm::vec2 cursor_2_position) {
      InputEvent::action = Pinch;
      InputEvent::cursor_position = cursor_1_position;
      InputEvent::second_cursor = cursor_2_position;
    }

    ShakeLeftEvent::ShakeLeftEvent() {
      InputEvent::action = Shake_Left;
    }

    ShakeRightEvent::ShakeRightEvent() {
      InputEvent::action = Shake_Right;
    }

    RotationSensorEvent::RotationSensorEvent(glm::quat rotation_) {
      InputEvent::action = Rotation_Sensor;
      InputEvent::rotation = rotation_;
    }
  }
}
