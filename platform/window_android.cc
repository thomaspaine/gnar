// Thomas Paine, 2024

#include "window_android.h"

namespace gnar {
  namespace platform {
    Window::Window(Platform &platform, std::string) :
      handle{platform.window},
      platform{platform} {
      spdlog::info("Create window");
      width = ANativeWindow_getWidth(handle);
      height = ANativeWindow_getHeight(handle);
      spdlog::info("Width: {}, Height: {}", width, height);

      android_window_should_close = false;
      orientation_changed = false;
    }

    bool Window::should_close() {
      return android_window_should_close;
    }

    void Window::set_orientation_changed(bool orientation_changed_) {
      orientation_changed = orientation_changed_;
    }

    void Window::process_events() {
      android_app *app = platform.get_app();

      // Orientation changed
      if(orientation_changed) {
	spdlog::info("Orientation changed!");
	orientation_changed = false;
      }

      android_input_buffer* input_buffer = android_app_swap_input_buffers(app);

      if (input_buffer && input_buffer->motionEventsCount) {
	//spdlog::info("Motion event(s) detected!");
	for(uint32_t i = 0; i < input_buffer->motionEventsCount; i++) {
	  GameActivityMotionEvent *motion_event = &input_buffer->motionEvents[i];
	  const int action_masked = motion_event->action & AMOTION_EVENT_ACTION_MASK;
	  uint32_t pointer_count = motion_event->pointerCount;
	  if(pointer_count == 1) {
	    // Drag
	    float x = GameActivityPointerAxes_getX(&motion_event->pointers[0]);
	    float y = GameActivityPointerAxes_getY(&motion_event->pointers[0]);
	    glm::vec2 cursor_position = {2.f*x/width - 1.f, 2.f*y/height - 1.f};

	    if(action_masked == AMOTION_EVENT_ACTION_DOWN) {
	      // Begin
	      spdlog::info("Drag begin; x: {}, y: {}", cursor_position.x, cursor_position.y);
	      platform.input_event(DragBeginEvent(cursor_position));
	    } else if(action_masked == AMOTION_EVENT_ACTION_MOVE) {
	      // Move
	      //spdlog::info("Drag move; x: {}, y: {}", cursor_position.x, cursor_position.y);
	      platform.input_event(DragMoveEvent(cursor_position));
	    } else if (action_masked == AMOTION_EVENT_ACTION_UP) {
	      // End
	      spdlog::info("Drag end; x: {}, y: {}", cursor_position.x, cursor_position.y);
	      platform.input_event(DragEndEvent(cursor_position));
	    }
	  } else if(pointer_count == 2) {
	    // Pinch
	    float x1 = GameActivityPointerAxes_getX(&motion_event->pointers[0]);
	    float y1 = GameActivityPointerAxes_getY(&motion_event->pointers[0]);
	    float x2 = GameActivityPointerAxes_getX(&motion_event->pointers[1]);
	    float y2 = GameActivityPointerAxes_getY(&motion_event->pointers[1]);
	    glm::vec2 cursor_1_position = {2.f*x1/width - 1.f, 2.f*y1/height - 1.f};
	    glm::vec2 cursor_2_position = {2.f*x2/width - 1.f, 2.f*y2/height - 1.f};

	    if(action_masked == AMOTION_EVENT_ACTION_MOVE) {
	      spdlog::info("Pinch move; (x1: {}, y1: {}), (x2: {}, y2: {})",
			   cursor_1_position.x, cursor_1_position.y,
			   cursor_2_position.x, cursor_2_position.y);
	      platform.input_event(PinchMoveEvent(cursor_1_position, cursor_2_position));
	    }
	  } else if(pointer_count == 3) {
	    //spdlog::info("3 cursors detected");
	  } else {
	    //spdlog::info("Cursor count greater than 3");
	  }
	}
	android_app_clear_motion_events(input_buffer);
      }
    }

    void Window::set_should_close() {
      android_window_should_close = true;
    }

    ANativeWindow &Window::get_handle() {
      return *handle;
    }
  }
}
