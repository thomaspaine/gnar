// Thomas Paine, 2024

#pragma once

#include <android/sensor.h>
#include <glm/gtc/quaternion.hpp>


#include "common/includes.h"

#include "platform/input_event.h"

namespace gnar {
  namespace platform {
    class Sensor {
    public:
      Sensor();

      std::vector<InputEvent> update();
    private:
      const int LOOPER_ID_USER = 3;
      const int SENSOR_REFRESH_RATE_HZ = 100;
      const int32_t SENSOR_REFRESH_PERIOD_US = int32_t(1000000 / SENSOR_REFRESH_RATE_HZ);

      const ASensor *rotationVectorSensor;
      ASensorManager *sensorManager;
      ASensorEventQueue *sensorEventQueue;
      ALooper *looper;

bool debug = false;

float w, x, y, z;

    };
  }
}
