// Thomas Paine, 2024

#include "platform_linux.h"

namespace gnar {
  namespace platform {
    Platform::Platform(PlatformOptions platform_options) :
      width{platform_options.width},
      height{platform_options.height},
      extensions{platform_options.extensions},
      layers{platform_options.layers}
    {

    }
    const std::vector<const char*> &Platform::get_required_extensions() const {
      return extensions;
    }
    const std::vector<const char*> &Platform::get_required_layers() const {
      return layers;
    }
    const uint32_t &Platform::get_width() const {
      return width;
    }
    const uint32_t &Platform::get_height() const {
      return height;
    }

    const std::vector<InputEvent> &Platform::get_input_events() const {
      return input_events;
    }

    const void Platform::clear_input_events() {
      input_events.resize(0);
    }

    void Platform::update() {
      //spdlog::info("Update called in platform");
    }

    void Platform::input_event(InputEvent event) {
      input_events.push_back(event);
    }
    // void Platform::set_width(uint32_t width_) {
    //   width = width_;
    // }
    // void Platform::set_height(uint32_t height_) {
    //   height = height_;
    // }
  }
}
