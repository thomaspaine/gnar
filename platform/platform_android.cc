// Thomas Paine, 2024

#include "platform_android.h"

namespace gnar {
  namespace platform {
    Platform::Platform(PlatformOptions platform_options, ANativeWindow *&window, AAssetManager *&asset_manager, android_app *app) :
      window{app->window},
      asset_manager{asset_manager},
      extensions{platform_options.extensions},
      layers{platform_options.layers},
      app{app}
    {
     sensor = create_sensor();
    }
    const std::vector<const char*> &Platform::get_required_extensions() const {
      return extensions;
    }
    const std::vector<const char*> &Platform::get_required_layers() const {
      return layers;
    }

    const std::vector<InputEvent> &Platform::get_input_events() const {
      return input_events;
    }

    const void Platform::clear_input_events() {
      input_events.resize(0);
    }

    void Platform::input_event(InputEvent event) {
      input_events.push_back(event);
    }

    void Platform::update() {
      std::vector<InputEvent> input_events = sensor->update();
      for(auto event : input_events) {
	input_event(event);
      }
    }

    AAssetManager *Platform::get_asset_manager() const {
      return asset_manager;
    }

    android_app *Platform::get_app() {
      return app;
    }

    void Platform::stop_sensors() {
      spdlog::warn("Platform::stop_senors(): I'm not doing anything.");
      // sensor.stop();
    }
    void Platform::start_sensors() {
      spdlog::warn("Platform::start_senors(): I'm not doing anything.");
      // sensor.stop();
    }

    std::unique_ptr<Sensor> Platform::create_sensor() {
      return std::make_unique<Sensor>();
    }
  }
}
