// Thomas Paine, 2024

#pragma once

#include "common/includes.h"

#include "platform/input_event.h"

#include <GLFW/glfw3.h>

namespace gnar {
  namespace platform {
    class Platform {
    public:
      struct PlatformOptions {
	std::vector<const char*> extensions;
	std::vector<const char*> layers;
	uint32_t width;
	uint32_t height;
      };
      Platform(PlatformOptions platform_options);

      void input_event(InputEvent event);
      void update();

      const uint32_t &get_width() const;
      const uint32_t &get_height() const;
      /* void set_width(uint32_t width_); */
      /* void set_height(uint32_t height_); */
      const std::vector<const char*> &get_required_extensions() const;
      const std::vector<const char*> &get_required_layers() const;

      const std::vector<InputEvent> &get_input_events() const;
      const void clear_input_events();
    private:
      std::vector<InputEvent> input_events;

      uint32_t width;
      uint32_t height;
      std::vector<const char*> extensions;
      std::vector<const char*> layers;
    };
  }
}
