// Thomas Paine, 2024

#pragma once

#include "common/includes.h"

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
//#include <string>

namespace gnar {
  namespace platform {
    class InputEvent {
    public:
      enum EventType { Mouse_Scroll, Mouse_Move,
		       Drag_Begin, Drag_Move, Drag_End,
		       Pinch, Shake_Left, Shake_Right, Rotation_Sensor,
		       Key_Left, Key_Right, Key_Up, Key_Down,
		       Release_Left, Release_Right, Release_Up, Release_Down,
		       Key_Esc
      };
      EventType action;
      bool scroll_up;
      glm::vec2 cursor_position;
      glm::vec2 second_cursor;
      glm::quat rotation;
    protected:
      //Action action;
    };

    // Mouse
    class MouseScrollEvent : public InputEvent {
    public:
      MouseScrollEvent(bool scroll_up);
    private:
      //float value;
    };
    class MouseMoveEvent : public InputEvent {
    public:
      MouseMoveEvent(float x, float y);
    };

    // Keyboard
    class KeyLeftPressEvent : public InputEvent {
    public:
      KeyLeftPressEvent();
    };
    class KeyRightPressEvent : public InputEvent {
    public:
      KeyRightPressEvent();
    };
    class KeyUpPressEvent : public InputEvent {
    public:
      KeyUpPressEvent();
    };
    class KeyDownPressEvent : public InputEvent {
    public:
      KeyDownPressEvent();
    };

    class KeyLeftReleaseEvent : public InputEvent {
    public:
      KeyLeftReleaseEvent();
    };
    class KeyRightReleaseEvent : public InputEvent {
    public:
      KeyRightReleaseEvent();
    };
    class KeyUpReleaseEvent : public InputEvent {
    public:
      KeyUpReleaseEvent();
    };
    class KeyDownReleaseEvent : public InputEvent {
    public:
      KeyDownReleaseEvent();
    };

    class KeyEscPressEvent : public InputEvent {
    public:
      KeyEscPressEvent();
    };


    // Drag
    class DragBeginEvent : public InputEvent {
    public:
      DragBeginEvent(glm::vec2 cursor_position_);
    };
    class DragMoveEvent : public InputEvent {
    public:
      DragMoveEvent(glm::vec2 cursor_position_);
    };
    class DragEndEvent : public InputEvent {
    public:
      DragEndEvent(glm::vec2 cursor_position_);
    };

    // Pinch
    class PinchMoveEvent : public InputEvent {
    public:
      PinchMoveEvent(glm::vec2 cursor_1_position, glm::vec2 cursor_2_position);
    };

    class ShakeLeftEvent : public InputEvent {
    public:
      ShakeLeftEvent();
    };
    class ShakeRightEvent : public InputEvent {
    public:
      ShakeRightEvent();
    };
    class RotationSensorEvent : public InputEvent {
    public:
      RotationSensorEvent(glm::quat rotation);
    };
}
}
