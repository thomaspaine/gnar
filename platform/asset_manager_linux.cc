// Thomas Paine, 2024
// Filesystem support

#include "asset_manager_linux.h"

namespace gnar {
  namespace platform {
    AssetManager::AssetManager(const Platform &platform) {
      spdlog::info("Creating Asset Manager");
      shader_path = "shaders";
      // for (const auto &entry : std::filesystem::directory_iterator(path)) {
      // 	spdlog::info("File:{}", entry.path().c_str());
      // }
    }

    std::vector<char> AssetManager::read_file(const std::string &filename) {
      std::string filepath = filename;
      std::ifstream file(filepath, std::ios::ate | std::ios::binary);
      if(!file.is_open()) {
	spdlog::error("Could not open file");
	throw;
      }
      size_t filesize = (size_t) file.tellg();
      std::vector<char> buffer(filesize);
      file.seekg(0);
      file.read(buffer.data(), filesize);
      file.close();
      return buffer;
    }

  }
}
