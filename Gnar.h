// Thomas Paine, 2024

// Vulkan Engine class header

#include "common/includes.h"

#include "platform/application.h"
#include "platform/window_delegator.h"

#include "core/instance.h"
#include "core/physical_device.h"
#include "core/device.h"
#include "core/surface.h"
#include "core/shader_module.h"
#include "core/buffer_context.h"

#include "render/render_context.h"

#include "component/camera.h"
#include "component/timer.h"

class Gnar : public gnar::platform::Application {
public:
  Gnar();

  void prepare();
  void update();
  void cleanup();

  bool prepared;
  bool destroy_requested;

protected:
  gnar::platform::Window &get_window();
  gnar::core::Instance &get_instance();
  gnar::core::PhysicalDevice &get_physical_device();
  gnar::core::Device &get_device();
  gnar::core::Surface &get_surface();
  gnar::render::RenderContext &get_render_context();
  gnar::component::Camera &get_camera();
  gnar::component::Timer &get_timer();
  gnar::platform::AssetManager &get_asset_manager();
  gnar::core::BufferContext &get_buffer_context();

  std::unique_ptr<gnar::platform::Window> create_window();
  std::unique_ptr<gnar::core::Instance> create_instance();
  std::unique_ptr<gnar::core::PhysicalDevice> create_physical_device();
  std::unique_ptr<gnar::core::Device> create_device();
  std::unique_ptr<gnar::core::Surface> create_surface();
  std::unique_ptr<gnar::render::RenderContext> create_render_context();
  std::unique_ptr<gnar::component::Camera> create_camera();
  std::unique_ptr<gnar::component::Timer> create_timer();
  std::unique_ptr<gnar::platform::AssetManager> create_asset_manager();
  std::unique_ptr<gnar::core::BufferContext> create_buffer_context();

private:
  std::unique_ptr<gnar::platform::Window> window;
  std::unique_ptr<gnar::core::Instance> instance;
  std::unique_ptr<gnar::core::PhysicalDevice> physical_device;
  std::unique_ptr<gnar::core::Device> device;
  std::unique_ptr<gnar::core::Surface> surface;
  std::unique_ptr<gnar::render::RenderContext> render_context;
  std::unique_ptr<gnar::component::Camera> camera;
  std::unique_ptr<gnar::component::Timer> timer;
  std::unique_ptr<gnar::platform::AssetManager> asset_manager;
  std::unique_ptr<gnar::core::BufferContext> buffer_context;
};
