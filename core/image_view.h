// Thomas Paine, 2024
// Image View header

#pragma once

//#include "common/helpers.h"

#include "core/device.h"

namespace gnar {
  namespace core {
    class ImageView {
    public:
      ImageView(Device &device, VkImage &image_handle, VkFormat &format, VkImageAspectFlagBits flags);
      ~ImageView();
      VkImageView get_handle() const;
    private:
      VkImageView handle{VK_NULL_HANDLE};
      Device &device;
    };
  }
}
