// Thomas Paine, 2024
// Descriptor set header

#pragma once

#include "common/includes.h"

#include "core/device.h"
#include "core/buffer_context.h"
#include "core/buffer.h"

#include "scene/scene_graph.h"

namespace gnar {
  namespace core {
    class DescriptorSet {
    public:
      DescriptorSet(Device &device, BufferContext &buffer_context, scene::SceneGraph &scene_graph,
		    uint32_t ubo_index);
      ~DescriptorSet();
      const VkDescriptorSet &get_handle() const;
      const VkDescriptorSetLayout &get_layout() const;
      //void update();
    private:
      VkDescriptorSet handle;
      VkDescriptorSetLayout layout;
      VkDescriptorPool descriptor_pool;
      VkWriteDescriptorSet write_descriptor_set;

      Device &device;
      Buffer &uniform_buffer;
    };
  }
}
