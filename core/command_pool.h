// Thomas Paine, 2024
// Command Pool header

#pragma once

#include "core/device.h"

namespace gnar {
  namespace core {
    class CommandPool {
    public:
      CommandPool(Device &device);
      ~CommandPool();
      VkCommandPool get_handle() const;
    private:
      VkCommandPool handle;
      Device &device;
    };
  }
}
