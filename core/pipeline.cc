// Thomas Paine, 2024
// Graphics pipeline wrapper

#include "pipeline.h"

// TODO MULTIPLE SHADERS

namespace gnar {
  namespace core {
    Pipeline::Pipeline(Device &device,
		       std::vector<std::unique_ptr<ShaderModule>> &shader_modules,
		       RenderPass &render_pass,
		       DescriptorSet &descriptor_set) : // TODO REMOVE unique_ptr in constructor
      device{device},
      shader_modules{shader_modules},
      render_pass{render_pass},
      descriptor_set{descriptor_set} {
      spdlog::info("Creating graphics pipeline");
      //      pipeline_state = create_pipeline_state();
    }

    Pipeline::~Pipeline() {
      vkDestroyPipelineLayout(device.get_handle(), pipeline_layout, nullptr);
      vkDestroyPipeline(device.get_handle(), handle, nullptr);
    }

    void Pipeline::prepare() {
      VkResult result;

      VkPipelineShaderStageCreateInfo vertex_shader_info = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
	.stage = VK_SHADER_STAGE_VERTEX_BIT,
	.module = shader_modules[0]->get_handle(),
	.pName = "main"
      };

      VkPipelineShaderStageCreateInfo fragment_shader_info = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
	.stage = VK_SHADER_STAGE_FRAGMENT_BIT,
	.module = shader_modules[1]->get_handle(),
	.pName = "main"
      };

      VkPipelineShaderStageCreateInfo shader_stages[] = {
	vertex_shader_info, fragment_shader_info
      };

      std::vector<VkDynamicState> dynamic_states = {
	VK_DYNAMIC_STATE_VIEWPORT,
	VK_DYNAMIC_STATE_SCISSOR
      };

      dynamic_info = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
	.dynamicStateCount = static_cast<uint32_t>(dynamic_states.size()),
	.pDynamicStates = dynamic_states.data()
      };

      VkPipelineVertexInputStateCreateInfo vertex_input_info = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
	.vertexBindingDescriptionCount = static_cast<uint32_t>(vertex_bindings.size()),
	.pVertexBindingDescriptions = vertex_bindings.data(),
	.vertexAttributeDescriptionCount = static_cast<uint32_t>(vertex_attributes.size()),
	.pVertexAttributeDescriptions = vertex_attributes.data()
      };

      VkPipelineInputAssemblyStateCreateInfo input_assembly_info = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
	.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST
      };

      VkPipelineViewportStateCreateInfo viewport_info = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
	.viewportCount = 1,
	.scissorCount = 1
      };

      VkPipelineRasterizationStateCreateInfo rasterisation_info = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
	.depthClampEnable = VK_FALSE,
	.polygonMode = polygon_mode,
	.cullMode = VK_CULL_MODE_BACK_BIT,
	.frontFace = VK_FRONT_FACE_CLOCKWISE,
	.lineWidth = 1.0f
      };

      VkPipelineMultisampleStateCreateInfo multisample_info = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
	.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT
      };

      VkPipelineColorBlendAttachmentState colour_blend_attachment = {
	.blendEnable = VK_TRUE,
	.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
	.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
	.colorBlendOp = VK_BLEND_OP_ADD,
	.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
	.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
	.alphaBlendOp = VK_BLEND_OP_ADD,
	.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |
	VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT
      };

      VkPipelineColorBlendStateCreateInfo colour_blend_info = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
	.attachmentCount = 1,
	.pAttachments = &colour_blend_attachment
      };

      depth_stencil_info = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
	.depthTestEnable = VK_TRUE,
	.depthWriteEnable = VK_TRUE,
	.depthCompareOp = VK_COMPARE_OP_LESS
      };

      //VkDescriptorSetLayout descriptor_set_layouts[] = { descriptor_set.get_layout() };

      VkPipelineLayoutCreateInfo pipeline_layout_info = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
	// TODO MULTIPLE DESCRIPTOR SETS
	.setLayoutCount = 1,
	.pSetLayouts = &descriptor_set.get_layout()
      };

      result = vkCreatePipelineLayout(device.get_handle(), &pipeline_layout_info, nullptr, &pipeline_layout);
      if(result != VK_SUCCESS) {
	spdlog::error("Could not create pipeline layout");
	throw;
      }
      spdlog::info("Created pipeline layout");

      pipeline_info = {
	.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
	.stageCount = 2,
	.pStages = shader_stages,
	.pVertexInputState = &vertex_input_info,
	.pInputAssemblyState = &input_assembly_info,
	.pViewportState = &viewport_info,
	.pRasterizationState = &rasterisation_info,
	.pMultisampleState = &multisample_info,
	.pDepthStencilState = &depth_stencil_info,
	.pColorBlendState = &colour_blend_info,
	.pDynamicState = &dynamic_info,
	.layout = pipeline_layout,
	.renderPass = render_pass.get_handle(),
	.subpass = 0
      };

      result = vkCreateGraphicsPipelines(device.get_handle(), VK_NULL_HANDLE, 1, &pipeline_info, nullptr, &handle);
      if(result != VK_SUCCESS) {
	spdlog::error("Could not create graphics pipeline");
	throw;
      }
      spdlog::info("Created graphics pipeline");


      for(std::unique_ptr<ShaderModule> &shader_module : shader_modules) {
	vkDestroyShaderModule(device.get_handle(), shader_module->get_handle(), nullptr);
      }
    }

    void Pipeline::add_vertex_binding(uint32_t vertex_size) {
      // TODO VkVertexInputBindingDescription
      spdlog::info("Binding vertex with size: {}", vertex_size);
      VkVertexInputBindingDescription binding = {
	.binding = 0,
	.stride = vertex_size,
	.inputRate = VK_VERTEX_INPUT_RATE_VERTEX
      };
      vertex_bindings.push_back(binding);
    }

    void Pipeline::add_vertex_attribute(uint32_t location, VkFormat format, uint32_t offset) {
      // TODO VkVertexInputAttributeDescription
      spdlog::info("Adding attribute at offset: {}", offset);
      VkVertexInputAttributeDescription attribute = {
	.location = location,
	.binding = 0,
	.format = format,
	.offset = offset
      };
      vertex_attributes.push_back(attribute);
    }

    const VkPipeline &Pipeline::get_handle() const {
      return handle;
    }

    const VkPipelineLayout &Pipeline::get_layout() const {
      return pipeline_layout;
    }

    std::unique_ptr<render::PipelineState> Pipeline::create_pipeline_state() {
      return std::make_unique<render::PipelineState>();
    }
  }
}
