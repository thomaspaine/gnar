// Thomas Paine, 2024
// Buffer builder header
// This class allocates memory and manages the buffers that are used to send data to shaders

#pragma once

#include "common/includes.h"

#include "core/device.h"
#include "core/buffer.h"

namespace gnar {
  namespace core {
    class BufferContext {
    public:
      BufferContext(Device &device);
      //UniformBuffer &get_uniform_buffer();
      Buffer &get_buffer(uint32_t index);
      //void build_uniform_buffer(VkDeviceSize buffer_size);

      uint32_t build_buffer(VkDeviceSize buffer_size, VkBufferUsageFlagBits usage);
    private:
      Device &device;
      //std::unique_ptr<UniformBuffer> uniform_buffer;
      std::vector<std::unique_ptr<Buffer>> buffers;
    };
  }
}
