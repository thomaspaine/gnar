// Thomas Paine, 2024
// Uniform Buffer Object header

#pragma once

#include "common/includes.h"

#include "core/device.h"

namespace gnar {
  namespace core {
    class Buffer {
    public:
      Buffer(Device &device, VkDeviceSize buffer_size, VkBufferUsageFlagBits buffer_usage);
      ~Buffer();
      void map_memory(void *data, VkDeviceSize data_size);
      VkBuffer get_handle() const;
      VkDeviceSize &get_size();
    private:
      VkBuffer handle;
      VkDeviceMemory buffer_memory;
      void *buffer_mapped;
      VkDeviceSize buffer_size;

      Device &device;
    };

    // class UniformBuffer {
    // public:
    //   UniformBuffer(Device &device, VkDeviceSize buffer_size);
    //   ~UniformBuffer();
    //   //void UniformBuffer::get_mapped_memory();
    //   void map_memory(void *data);
    //   VkBuffer get_handle() const;
    //   VkDeviceSize &get_size();
    // private:
    //   VkBuffer handle;
    //   VkDeviceMemory buffer_memory;
    //   void *buffer_mapped;
    //   VkDeviceSize buffer_size;

    //   Device &device;
    // };
  }
}
