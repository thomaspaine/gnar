// Thomas Paine, 2024
// VkDevice header
// Wrapper for logical device

#pragma once

#include "core/physical_device.h"

namespace gnar {
  namespace core {
    class Device {
    public:
      Device(PhysicalDevice &gpu);
      ~Device();
      VkDevice get_handle() const;
      const PhysicalDevice &get_gpu() const;
      const VkQueue &get_graphics_queue() const;
      const VkQueue &get_present_queue() const;
      uint32_t get_memory_type(uint32_t bits, VkMemoryPropertyFlags flags);
      VkFormat &get_depth_format();
    private:
      VkDevice handle;
      PhysicalDevice &gpu;
      VkQueue graphics_queue;
      VkQueue present_queue;

      VkFormat depth_format;
    };
  }
}
