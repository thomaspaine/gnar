// Thomas Paine
// Wrapper for uniform buffer

#include "buffer.h"

namespace gnar {
  namespace core {
    Buffer::Buffer(Device &device, VkDeviceSize buffer_size, VkBufferUsageFlagBits buffer_usage) :
      device{device},
      buffer_size{buffer_size} {
      spdlog::info("Creating buffer");

      VkResult result;

      VkBufferCreateInfo buffer_info = {
	.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
	.size = buffer_size,
	.usage = buffer_usage,
	.sharingMode = VK_SHARING_MODE_EXCLUSIVE
      };

      result = vkCreateBuffer(device.get_handle(), &buffer_info, nullptr, &handle);
      if(result != VK_SUCCESS) {
	spdlog::error("Could not create buffer");
	throw;
      }
      spdlog::info("Created buffer");

      VkMemoryRequirements memory_requirements;
      vkGetBufferMemoryRequirements(device.get_handle(), handle, &memory_requirements);
      VkMemoryPropertyFlags flags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
      VkMemoryAllocateInfo buffer_memory_info = {
	.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
	.allocationSize = memory_requirements.size,
	.memoryTypeIndex = device.get_memory_type(memory_requirements.memoryTypeBits, flags)
      };

      vkAllocateMemory(device.get_handle(), &buffer_memory_info, nullptr, &buffer_memory);
      vkBindBufferMemory(device.get_handle(), handle, buffer_memory, 0);
      vkMapMemory(device.get_handle(), buffer_memory, 0, buffer_size, 0, &buffer_mapped);

      spdlog::info("Mapped buffer memory");
    }

    Buffer::~Buffer() {
      spdlog::info("Destroying buffer");
      vkDestroyBuffer(device.get_handle(), handle, nullptr);
      vkFreeMemory(device.get_handle(), buffer_memory, nullptr);
    }

    VkBuffer Buffer::get_handle() const {
      return handle;
    }

    void Buffer::map_memory(void *data, VkDeviceSize data_size) {
      memcpy(buffer_mapped, data, data_size);
    }

    VkDeviceSize &Buffer::get_size() {
      return buffer_size;
    }

    // UniformBuffer::UniformBuffer(Device &device, VkDeviceSize buffer_size) :
    //   device{device}, buffer_size{buffer_size} {
    //   spdlog::info("Creating uniform buffer");
    //   VkResult result;

    //   VkBufferCreateInfo buffer_info = {
    // 	.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
    // 	.size = buffer_size,
    // 	.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
    // 	.sharingMode = VK_SHARING_MODE_EXCLUSIVE
    //   };

    //   result = vkCreateBuffer(device.get_handle(), &buffer_info, nullptr, &handle);
    //   if(result != VK_SUCCESS) {
    // 	spdlog::error("Could not create buffer");
    // 	throw;
    //   }
    //   spdlog::info("Created buffer");

    //   VkMemoryRequirements memory_requirements;
    //   vkGetBufferMemoryRequirements(device.get_handle(), handle, &memory_requirements);
    //   VkMemoryPropertyFlags flags = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT;
    //   VkMemoryAllocateInfo buffer_memory_info = {
    // 	.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
    // 	.allocationSize = memory_requirements.size,
    // 	.memoryTypeIndex = device.get_memory_type(memory_requirements.memoryTypeBits, flags)
    //   };

    //   vkAllocateMemory(device.get_handle(), &buffer_memory_info, nullptr, &buffer_memory);
    //   vkBindBufferMemory(device.get_handle(), handle, buffer_memory, 0);
    //   vkMapMemory(device.get_handle(), buffer_memory, 0, buffer_size, 0, &buffer_mapped);
    // }
    // UniformBuffer::~UniformBuffer() {
    //   spdlog::info("Destroying buffer");
    //   vkDestroyBuffer(device.get_handle(), handle, nullptr);
    //   vkFreeMemory(device.get_handle(), buffer_memory, nullptr);
    // }
    // // void *UniformBuffer::get_mapped_memory() {
    // //   return *buffer_mapped;
    // // }
    // void UniformBuffer::map_memory(void *data) {
    //   memcpy(buffer_mapped, data, buffer_size);
    // }

    // VkBuffer UniformBuffer::get_handle() const {
    //   return handle;
    // }

    // VkDeviceSize &UniformBuffer::get_size() {
    //   return buffer_size;
    // }
  }
}
