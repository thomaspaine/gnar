// Thomas Paine, 2024
// Graphics pipeline header

#pragma once

#include "common/includes.h"

#include "core/device.h"
#include "core/shader_module.h"
#include "core/render_pass.h"
#include "core/descriptor_set.h"

#include "render/pipeline_state.h"

namespace gnar {
  namespace core {
    class Pipeline {
    public:
      Pipeline(Device &device, std::vector<std::unique_ptr<ShaderModule>> &shader_modules, RenderPass &render_pass, DescriptorSet &descriptor_set);
      ~Pipeline();

      void prepare();

      const VkPipeline &get_handle() const;
      const VkPipelineLayout &get_layout() const;

      void add_vertex_binding(uint32_t vertex_size);
      void add_vertex_attribute(uint32_t index, VkFormat format, uint32_t offset);

      void set_polygon_mode(VkPolygonMode polygon_mode_);

    private:
      VkPolygonMode polygon_mode = VK_POLYGON_MODE_FILL;

      std::vector<VkVertexInputBindingDescription> vertex_bindings{};
      std::vector<VkVertexInputAttributeDescription> vertex_attributes{};

      VkPipeline handle;
      VkPipelineLayout pipeline_layout;

      VkGraphicsPipelineCreateInfo pipeline_info;

      Device &device;
      RenderPass &render_pass;
      DescriptorSet &descriptor_set;

      std::unique_ptr<render::PipelineState> pipeline_state;
      std::unique_ptr<render::PipelineState> create_pipeline_state();

      VkPipelineDepthStencilStateCreateInfo depth_stencil_info;
      VkPipelineDynamicStateCreateInfo dynamic_info;
      std::vector<VkDynamicState> dynamic_states;

      std::vector<std::unique_ptr<ShaderModule>> &shader_modules;
    };
  }
}
