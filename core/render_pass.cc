// Thomas Paine, 2024
// Render pass wrapper

#include "render_pass.h"

namespace gnar {
  namespace core {
    RenderPass::RenderPass(Device &device, VkFormat &format) :
      device{device} {
      spdlog::info("Creating render pass");
      VkResult result;

      VkAttachmentDescription colour_attachment{
	.format = format,
	.samples = VK_SAMPLE_COUNT_1_BIT,
	.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
	.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
	.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
	.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
	.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
	.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
      };

      VkAttachmentReference colour_attachment_ref{
	.attachment = 0,
	.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
      };

      VkAttachmentDescription depth_attachment{
	.format = device.get_depth_format(),
	.samples = VK_SAMPLE_COUNT_1_BIT,
	.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
	.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
	.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
	.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
	.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
	.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
      };

      VkAttachmentReference depth_attachment_ref{
	.attachment = 1,
	.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
      };

      VkSubpassDescription subpass{
	.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
	.colorAttachmentCount = 1,
	.pColorAttachments = &colour_attachment_ref,
	.pDepthStencilAttachment = &depth_attachment_ref
      };
      VkSubpassDependency dependency{
	.srcSubpass = VK_SUBPASS_EXTERNAL,
	.dstSubpass = 0,
	.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
	.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
	.srcAccessMask = 0,
	.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT
      };

      VkAttachmentDescription attachments[] = {colour_attachment, depth_attachment};

      VkRenderPassCreateInfo create_info{
	.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
	.attachmentCount = 2,
	.pAttachments = attachments,
	.subpassCount = 1,
	.pSubpasses = &subpass,
	.dependencyCount = 1,
	.pDependencies = &dependency
      };

      result = vkCreateRenderPass(device.get_handle(), &create_info, nullptr, &handle);
      if(result != VK_SUCCESS) {
	spdlog::error("Could not create render pass");
	throw;
      }
      spdlog::info("Created render pass");
    }
    RenderPass::~RenderPass() {
      spdlog::info("Destroying render pass");
      vkDestroyRenderPass(device.get_handle(), handle, nullptr);
    }

    VkRenderPass RenderPass::get_handle() const {
      return handle;
    }
  }
}
