// Thomas Paine, 2024
// VkPhysicalDevice wrapper
// PhysicalDevice corresponds to graphics hardware
// This class will be available from the logical device

#include "physical_device.h"

namespace gnar {
  namespace core {
    PhysicalDevice::PhysicalDevice(Instance &instance, Surface &surface) :
      instance{instance},
      surface{surface} {
      // Set VkPhysicalDevice handle
      pick_gpu();

      vkGetPhysicalDeviceFeatures(handle, &features);
      vkGetPhysicalDeviceProperties(handle, &properties);
      vkGetPhysicalDeviceMemoryProperties(handle, &memory_properties);

      spdlog::info("Picked GPU: {}", properties.deviceName);

      // Queue family properties
      uint32_t queue_family_properties_count = 0;
      vkGetPhysicalDeviceQueueFamilyProperties(handle, &queue_family_properties_count, nullptr);
      spdlog::info("counted {} queue family properties", queue_family_properties_count);
      queue_family_properties = std::vector<VkQueueFamilyProperties>(queue_family_properties_count);
      vkGetPhysicalDeviceQueueFamilyProperties(handle, &queue_family_properties_count, queue_family_properties.data());

      VkResult result;
      uint32_t device_extension_count = 0;
      result = vkEnumerateDeviceExtensionProperties(handle, nullptr, &device_extension_count, nullptr);
      std::vector<VkExtensionProperties> device_extensions = std::vector<VkExtensionProperties>(device_extension_count);
      result = vkEnumerateDeviceExtensionProperties(handle, nullptr, &device_extension_count, device_extensions.data());

      // Print extensions
      // for(auto &extension : device_extensions) {
      // 	spdlog::info(" - {}", extension.extensionName);
      // }

      // Queue family indices
      int i = 0;
      for(auto &queue_family : queue_family_properties) {
	VkBool32 present_support = false;
	vkGetPhysicalDeviceSurfaceSupportKHR(handle, i, surface.get_handle(), &present_support);
	if(present_support) {
	  indices.present_family = i;
	}
	if(queue_family.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
	  indices.graphics_family = i;
	}
	i++;
      }
    }

    // Set handle to discrete GPU or first in list
    void PhysicalDevice::pick_gpu() {
      uint32_t gpu_count = 0;
      VkResult result;
      result = vkEnumeratePhysicalDevices(instance.get_handle(), &gpu_count, nullptr);
      if(result != VK_SUCCESS) {
	spdlog::error("Could not count gpus");
	throw;
      }

      if(gpu_count == 0) {
	spdlog::error("No gpus found");
	throw;
      }

      spdlog::info("Found {} gpus", gpu_count);
      std::vector<VkPhysicalDevice> gpus(gpu_count);
      vkEnumeratePhysicalDevices(instance.get_handle(), &gpu_count, gpus.data());
      for(const auto gpu : gpus) {
	vkGetPhysicalDeviceProperties(gpu, &properties);
	spdlog::info("Checking device: {}", properties.deviceName);
	if(properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
	  handle = gpu;
	  break;
	}
      }
      if(handle == VK_NULL_HANDLE) {
	spdlog::info("Picked default GPU");
	handle = gpus[0];
      }
    }

    bool PhysicalDevice::is_device_suitable(VkPhysicalDevice physical_device) {
      return false;
    }

    VkPhysicalDevice PhysicalDevice::get_handle() const {
      return handle;
    }
    const VkPhysicalDeviceFeatures &PhysicalDevice::get_device_features() const {
      return features;
    }
    const std::vector<VkQueueFamilyProperties> &PhysicalDevice::get_queue_family_properties() const {
      return queue_family_properties;
    }
    const VkPhysicalDeviceMemoryProperties &PhysicalDevice::get_memory_properties() const {
      return memory_properties;
    }
    const QueueFamilyIndices &PhysicalDevice::get_indices() const {
      return indices;
    }
  }
}
