// Thomas Paine, 2024
// Synchronisation objects

#include "sync.h"

namespace gnar {
  namespace core {
    Sync::Sync(Device &device) :
      device{device} {
      spdlog::info("Creating synchronisation objects");
      VkResult result;

      VkFenceCreateInfo fence_info = {
	.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
	.flags = VK_FENCE_CREATE_SIGNALED_BIT
      };

      vkCreateFence(device.get_handle(), &fence_info, nullptr, &fence);

      VkSemaphoreCreateInfo semaphore_info = {
	.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO
      };

      vkCreateSemaphore(device.get_handle(), &semaphore_info, nullptr, &ready_semaphore);
      vkCreateSemaphore(device.get_handle(), &semaphore_info, nullptr, &finished_semaphore);
    }

    Sync::~Sync() {
      vkDestroyFence(device.get_handle(), fence, nullptr);
      vkDestroySemaphore(device.get_handle(), ready_semaphore, nullptr);
      vkDestroySemaphore(device.get_handle(), finished_semaphore, nullptr);
    }

    VkFence &Sync::get_fence() {
      return fence;
    }

    VkSemaphore &Sync::get_ready_semaphore() {
      return ready_semaphore;
    }
    VkSemaphore &Sync::get_finished_semaphore() {
      return finished_semaphore;
    }
  }
}
