// Thomas Paine, 2024
// Logical device wrapper for VkDevice

#include "device.h"

namespace gnar {
  namespace core {
    Device::Device(PhysicalDevice &gpu) :
      gpu{gpu},
      depth_format{VK_FORMAT_D32_SFLOAT}
    {
      spdlog::info("Creating logical device");
      VkResult result;

      // TODO queue priotities from physical device
      uint32_t queue_family_properties_count = gpu.get_queue_family_properties().size();
      spdlog::info("Queue family count: {}", queue_family_properties_count);
      std::vector<VkDeviceQueueCreateInfo> queue_create_infos(queue_family_properties_count);
      std::vector<std::vector<float>> queue_priorities(queue_family_properties_count);
      for(uint32_t queue_family_index = 0; queue_family_index < queue_family_properties_count; queue_family_index++) {
	spdlog::info("Queue family index: {}", queue_family_index);
	const VkQueueFamilyProperties &queue_family_property = gpu.get_queue_family_properties()[queue_family_index];
	VkDeviceQueueCreateInfo &queue_create_info = queue_create_infos[queue_family_index];
	queue_priorities[queue_family_index].resize(queue_family_property.queueCount, 0.5f);
	//spdlog::info(" - Queue priority: {}", queue_priorities[queue_family_index][0]);
	queue_create_info = {
	  .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
	  .queueCount = gpu.get_queue_family_properties()[queue_family_index].queueCount,
	  .pQueuePriorities = queue_priorities[queue_family_index].data()
	};
      }

      const std::vector<const char*> device_extensions = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME
      };

      const VkPhysicalDeviceFeatures device_features = {
	.fillModeNonSolid = VK_FALSE,
	.shaderFloat64 = VK_FALSE
      };

      VkDeviceCreateInfo device_info{
	.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
	.queueCreateInfoCount = static_cast<uint32_t>(queue_create_infos.size()),
	.pQueueCreateInfos = queue_create_infos.data(),
	.enabledExtensionCount = static_cast<uint32_t>(device_extensions.size()),
	.ppEnabledExtensionNames = device_extensions.data(),
	.pEnabledFeatures = &device_features
      };

      result = vkCreateDevice(gpu.get_handle(), &device_info, nullptr, &handle);
      if (result != VK_SUCCESS) {
	spdlog::error("Could not create logical device");
	throw;
      }
      spdlog::info("Created logical device");

      QueueFamilyIndices indices = gpu.get_indices();
      vkGetDeviceQueue(handle, indices.graphics_family, 0, &graphics_queue);
      vkGetDeviceQueue(handle, indices.present_family, 0, &present_queue);
    }

    Device::~Device() {
      vkDestroyDevice(handle, nullptr);
      spdlog::info("Destroyed device");
    }

    VkDevice Device::get_handle() const {
      return handle;
    }

    const PhysicalDevice &Device::get_gpu() const {
      return gpu;
    }

    const VkQueue &Device::get_graphics_queue() const {
      return graphics_queue;
    }

    const VkQueue &Device::get_present_queue() const {
      return present_queue;
    }

    uint32_t Device::get_memory_type(uint32_t bits, VkMemoryPropertyFlags flags) {
      VkPhysicalDeviceMemoryProperties memory_properties = gpu.get_memory_properties();
      uint32_t memory_type_count = memory_properties.memoryTypeCount;
      // spdlog::info("memory types: {}", memory_type_count);
      // spdlog::info("required flags: {}", flags);
      for(uint32_t i = 0; i < memory_type_count; i++) {
	//spdlog::info("property flags: {}", memory_properties.memoryTypes[i].propertyFlags);
       	if((bits & (1 << i)) && (memory_properties.memoryTypes[i].propertyFlags & flags) == flags) {
       	  return i;
       	}
      }
      spdlog::error("No valid memory type");
      throw;
    }

    VkFormat &Device::get_depth_format() {
      // TODO from priority list
      return depth_format;
    }
  }
}
