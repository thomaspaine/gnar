// Thomas Paine, 2024
// VkSurfaceKHR wrapper
// Surface properties to interact with app window

#include "surface.h"

namespace gnar {
  namespace core {
    // Constructor
    Surface::Surface(Instance &instance, platform::Window &window) :
      instance{instance}, window{window} {
      spdlog::info("Creating Surface");
      VkResult result;
#ifdef ANDROID
      VkAndroidSurfaceCreateInfoKHR android_surface_info{
       	.sType = VK_STRUCTURE_TYPE_ANDROID_SURFACE_CREATE_INFO_KHR,
	.window = &window.get_handle()
      };
      result = vkCreateAndroidSurfaceKHR(instance.get_handle(), &android_surface_info, nullptr, &handle);
#else
      result = glfwCreateWindowSurface(instance.get_handle(), &window.get_handle(), nullptr, &handle);
#endif
      if(result != VK_SUCCESS) {
	spdlog::error("Could not create surface");
	throw;
      }
      spdlog::info("Created surface");
    }

    // Destructor
    Surface::~Surface() {
      spdlog::info("Destroying surface");
      vkDestroySurfaceKHR(instance.get_handle(), handle, nullptr);
    }

    // Public method to access handle
    VkSurfaceKHR Surface::get_handle() const {
      return handle;
    }
  }
}
