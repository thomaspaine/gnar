// Thomas Paine, 2024
// Framebuffer header

#pragma once

#include "core/device.h"
#include "core/swapchain.h"
#include "core/image_view.h"
#include "core/render_pass.h"

#include "render/render_target.h"

namespace gnar {
  namespace core {
    class Framebuffer {
    public:
      Framebuffer(Device &device, Swapchain &swapchain, ImageView &image_view, RenderPass &render_pass, render::RenderTarget &render_target);
      ~Framebuffer();
      const VkFramebuffer &get_handle() const;
    private:
      VkFramebuffer handle;
      Device &device;
    };
  }
}
