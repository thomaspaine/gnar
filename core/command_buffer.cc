// Thomas Paine, 2024
// Command buffer wrapper

#include "command_buffer.h"

namespace gnar {
  namespace core {
    CommandBuffer::CommandBuffer(Device &device,
				 Swapchain &swapchain,
				 CommandPool &command_pool,
				 RenderPass &render_pass,
				 std::vector<DrawBuffer> &draw_buffers,
				 BufferContext &buffer_context,
				 scene::SceneGraph &scene_graph) :
      device{device},
      render_pass{render_pass},
      swapchain{swapchain},
      command_pool{command_pool},
      draw_buffers{draw_buffers},
      buffer_context{buffer_context} {
      spdlog::info("Creating command buffer");
      VkResult result;

      VkCommandBufferAllocateInfo command_buffer_info = {
	.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
	.commandPool = command_pool.get_handle(),
	.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
	.commandBufferCount = 1
      };

      result = vkAllocateCommandBuffers(device.get_handle(), &command_buffer_info, &handle);
      if(result != VK_SUCCESS) {
	spdlog::error("Could not allocate command buffers");
	throw;
      }
      spdlog::info("Created command buffer");

      begin();
      uint32_t texture_count = scene_graph.get_texture_count();
      for(uint32_t i = 0; i < texture_count; i++) {
	scene::Texture &texture = scene_graph.get_texture(i);
	VkImage &texture_image = texture.get_image();
	Buffer &staging_buffer = buffer_context.get_buffer(texture.get_staging_index());

	transition_image_layout(texture_image,
				VK_FORMAT_R8G8B8A8_SRGB,
				VK_IMAGE_LAYOUT_UNDEFINED,
				VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
	copy_buffer_to_image(staging_buffer.get_handle(), texture_image, texture.get_width(), texture.get_height());
	transition_image_layout(texture_image,
				VK_FORMAT_R8G8B8A8_SRGB,
				VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
				VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
      }

      end();
      submit();
    }

    void CommandBuffer::submit() {
      VkSubmitInfo submit_info{
	.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
	.commandBufferCount = 1,
	.pCommandBuffers = &handle,
      };

      vkQueueSubmit(device.get_graphics_queue(), 1, &submit_info, VK_NULL_HANDLE);
      vkQueueWaitIdle(device.get_graphics_queue());
    }

    void CommandBuffer::begin() {
      VkResult result;
      VkCommandBufferBeginInfo begin_info = {
	.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
	.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
      };
      result = vkBeginCommandBuffer(handle, &begin_info);
      if(result != VK_SUCCESS) {
	spdlog::error("Could not begin command buffer recording");
      }
    }

    void CommandBuffer::end() {
      VkResult result = vkEndCommandBuffer(handle);
      if(result != VK_SUCCESS) {
	spdlog::error("Could not end recording command buffer");
	throw;
      }
    }

    VkCommandBuffer CommandBuffer::get_handle() const {
      return handle;
    }

    void CommandBuffer::record(Framebuffer &framebuffer, Pipeline &pipeline, DescriptorSet &descriptor_set) {
      //spdlog::info("Recording command buffer");
      //descriptor_set.update();

      VkResult result;
      begin();
      //spdlog::info("Began recording command buffer");
      std::array<VkClearValue, 2> clear_values{};
      clear_values[0].color = {{0.f, 0.f, 0.f, 1.f}};
      clear_values[1].color = {1.f, 0.f};

      VkRenderPassBeginInfo render_pass_info = {
	.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
	.renderPass = render_pass.get_handle(),
	.framebuffer = framebuffer.get_handle(),
	.renderArea = {
	  .offset = {0, 0},
	  .extent = swapchain.get_extent()
	},
	.clearValueCount = clear_values.size(),
	.pClearValues = clear_values.data()
      };

      vkCmdBeginRenderPass(handle, &render_pass_info, VK_SUBPASS_CONTENTS_INLINE);
      vkCmdBindPipeline(handle, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.get_handle());
      vkCmdBindDescriptorSets(handle, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline.get_layout(), 0, 1, &descriptor_set.get_handle(), 0, NULL);

      for(DrawBuffer draw_buffer : draw_buffers) {

      VkBuffer vertex_buffers[] = { buffer_context.get_buffer(draw_buffer.vertex_buffer_index).get_handle() };
      VkDeviceSize offsets[] = {0};
      vkCmdBindVertexBuffers(handle, 0, 1, vertex_buffers, offsets);

      VkBuffer index_buffer = buffer_context.get_buffer(draw_buffer.index_buffer_index).get_handle();
      vkCmdBindIndexBuffer(handle, index_buffer, 0, VK_INDEX_TYPE_UINT16);

      VkViewport viewport = {
	.x = 0.0f,
	.y = 0.0f,
	.width = (float) swapchain.get_extent().width,
	.height = (float) swapchain.get_extent().height,
	.minDepth = 0.0f,
	.maxDepth = 1.0f
      };
      vkCmdSetViewport(handle, 0, 1, &viewport);

      VkRect2D scissor = {
	.offset = {0, 0},
	.extent = swapchain.get_extent()
      };
      vkCmdSetScissor(handle, 0, 1, &scissor);

      vkCmdDrawIndexed(handle, draw_buffer.indices_count, 1, 0, 0, 0);
      }
      vkCmdEndRenderPass(handle);
      end();
      //spdlog::info("Command buffer finished recording.");
    }

    void CommandBuffer::transition_image_layout(VkImage image, VkFormat format, VkImageLayout from, VkImageLayout to) {
      VkPipelineStageFlags source_stage;
      VkPipelineStageFlags destination_stage;

      VkAccessFlags source_flags;
      VkAccessFlags destination_flags;

      if(from == VK_IMAGE_LAYOUT_UNDEFINED && to == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
	source_flags = 0;
	destination_flags = VK_ACCESS_TRANSFER_WRITE_BIT;

	source_stage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
	destination_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
      } else if(from == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && to == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
	source_flags = VK_ACCESS_TRANSFER_WRITE_BIT;
	destination_flags = VK_ACCESS_SHADER_READ_BIT;

	source_stage = VK_PIPELINE_STAGE_TRANSFER_BIT;
	destination_stage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
      } else {
	spdlog::error("Invalid image transition");
	throw;
      }

      VkImageMemoryBarrier barrier{
	.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
	.srcAccessMask = source_flags,
	.dstAccessMask = destination_flags,
	.oldLayout = from,
	.newLayout = to,
	.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
	.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
	.image = image,
	.subresourceRange = {
	  .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
	  .baseMipLevel = 0,
	  .levelCount = 1,
	  .baseArrayLayer = 0,
	  .layerCount = 1
	}
      };
      vkCmdPipelineBarrier(handle, source_stage, destination_stage, 0, 0, nullptr, 0, nullptr, 1, &barrier);
    }

    void CommandBuffer::copy_buffer_to_image(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height) {
      VkBufferImageCopy region{
	.bufferOffset = 0,
	.bufferRowLength = 0,
	.bufferImageHeight = 0,
	.imageSubresource = {
	  .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
	  .mipLevel = 0,
	  .baseArrayLayer = 0,
	  .layerCount = 1
	},
	.imageOffset = {0, 0, 0},
	.imageExtent = {width, height, 1}
      };

      vkCmdCopyBufferToImage(handle,
			     buffer,
			     image,
			     VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			     1,
			     &region);
    }
  }
}
