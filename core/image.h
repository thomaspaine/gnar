// Thomas Paine, 2024
// Image header

namespace gnar {
  namespace core {
    class Image {
    public:
      Image();
      ~Image();

      VkImage &get_image() const;
    private:
      VkImage handle;
    }
  }
}
