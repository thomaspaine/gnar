// Thomas Paine, 2024
// Swapchain

#pragma once

#include "common/includes.h"

#include "core/device.h"
#include "core/surface.h"

namespace gnar {
  namespace core {
    class Swapchain {
    public:
      Swapchain(Device &device, Surface &surface);
      ~Swapchain();
      std::vector<VkImage> get_images();
      VkFormat &get_format();
      VkExtent2D &get_extent();
      VkSurfaceTransformFlagBitsKHR get_transform() const;
      VkSwapchainKHR get_handle() const;

      void recreate();
    private:
      VkSwapchainKHR handle;
      Device &device;
      Surface &surface;

      std::vector<VkImage> images;

      // Surface format
      VkSurfaceFormatKHR surface_format;
      // Present mode
      VkPresentModeKHR present_mode;
      // Surface capabilities
      VkSurfaceCapabilitiesKHR surface_capabilities;
      // Extent
      VkExtent2D extent;
      // Available surface formats
      std::vector<VkSurfaceFormatKHR> surface_formats;
      // Available present modes
      std::vector<VkPresentModeKHR> present_modes;
      VkSurfaceTransformFlagBitsKHR surface_transform;

      VkResult set_surface_formats();
      VkResult set_present_modes();
      VkResult set_surface_capabilities();

      void choose_surface_format();
      void choose_present_mode();
      void choose_extent();
    };
  }
}
