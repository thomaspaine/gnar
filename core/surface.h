// Thomas Paine, 2024
// Surface header
// The Vulkan surface interacts with the windowing system
// Each platform will have different requirements for surface creation

#pragma once

#include "core/instance.h"
#include "platform/window_delegator.h"

namespace gnar {
  namespace core {
    class Surface {
    public:
      Surface(Instance &instance, platform::Window &window);
      ~Surface();

      VkSurfaceKHR get_handle() const;
    private:
      VkSurfaceKHR handle;
      Instance &instance;
      platform::Window &window;
    };
  }
}
