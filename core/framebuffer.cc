// Thomas Paine, 2024
// Framebuffer wrapper

#include "framebuffer.h"

namespace gnar {
  namespace core {
    Framebuffer::Framebuffer(Device &device, Swapchain &swapchain, ImageView &image_view, RenderPass &render_pass, render::RenderTarget &render_target) :
      device{device} {
      spdlog::info("Creating framebuffer");
      VkResult result;

      VkExtent2D extent = swapchain.get_extent();

      VkImageView attachments[] = { image_view.get_handle(), render_target.get_depth_image_view().get_handle() };

      VkFramebufferCreateInfo framebuffer_info = {
	.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
	.renderPass = render_pass.get_handle(),
	.attachmentCount = 2,
	.pAttachments = attachments,
	.width = extent.width,
	.height = extent.height,
	.layers = 1
      };

      result = vkCreateFramebuffer(device.get_handle(), &framebuffer_info, nullptr, &handle);
      if(result != VK_SUCCESS) {
	spdlog::error("Could not create frame buffer");
	throw;
      }
      spdlog::info("Created frame buffer");
    }

    Framebuffer::~Framebuffer() {
      vkDestroyFramebuffer(device.get_handle(), handle, nullptr);
      spdlog::info("Destroyed framebuffer");
    }

    const VkFramebuffer &Framebuffer::get_handle() const {
      return handle;
    }
  }
}
