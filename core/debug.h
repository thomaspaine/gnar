// Thomas Paine, 2024
// Relating to validation layer support

#pragma once

#include "common/includes.h"

namespace gnar {
  namespace core {
    class Debug {
    public:
      VkResult CreateDebugUtilsMessengerEXT(
        VkInstance instance,
	VkDebugUtilsMessengerCreateInfoEXT *pCreateInfo,
	VkAllocationCallbacks *pAllocator,
	VkDebugUtilsMessengerEXT *pDebugMessenger);
      void DestroyDebugUtilsMessengerEXT(
        VkInstance instance,
	VkDebugUtilsMessengerEXT debugMessenger,
	const VkAllocationCallbacks *pAllocator);
      static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
	VkDebugUtilsMessageTypeFlagsEXT messageType,
	const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
	void *pUserData);
      void setupDebugMessenger();
      void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT &createInfo);
#ifdef NDEBUG
      const bool enableValidationLayers = false;
#else
      const bool enableValidationLayers = true;
#endif
    };
  }
}
