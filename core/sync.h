// Thomas Paine, 2024
// Synchronisation objects, header file

#pragma once

#include "common/includes.h"

#include "core/device.h"

namespace gnar {
  namespace core {
    class Sync {
    public:
      Sync(Device &device);
      ~Sync();
      VkFence &get_fence();
      VkSemaphore &get_ready_semaphore();
      VkSemaphore &get_finished_semaphore();
    private:
      VkFence fence;
      VkSemaphore ready_semaphore;
      VkSemaphore finished_semaphore;
      Device &device;
    };
  }
}
