// Thomas Paine, 2024
// VkInstance wrapper
// Enable the required vulkan extensions and layers

#include "instance.h"

namespace gnar {
  namespace core {
    // Constructor
    Instance::Instance(const platform::Platform &platform,
		       const std::string &application_name,
		       const std::string &engine_name
		       ) {
      spdlog::info("Creating instance");
      VkResult result;

      // Count layers
      result = vkEnumerateInstanceLayerProperties(&layer_count, nullptr);
      if(result != VK_SUCCESS) {
	spdlog::error("Could not count layers");
	throw;
      }
      // Get available layers
      spdlog::info("Found {} layers", layer_count);
      available_layers.resize(layer_count);
      result = vkEnumerateInstanceLayerProperties(&layer_count, available_layers.data());
      if(result != VK_SUCCESS) {
	spdlog::error("Could not enumerate layers");
	throw;
      }
      // Print available layers
      for(const auto layer : available_layers) {
	spdlog::info(" - {}", layer.layerName);
      }

      // Count extensions
      result = vkEnumerateInstanceExtensionProperties(nullptr, &extension_count, nullptr);
      if(result != VK_SUCCESS) {
	spdlog::error("Could not count extensions");
	throw;
      }
      spdlog::info("Found {} extensions", extension_count);

      // Get available extensions
      available_extensions.resize(extension_count);
      result = vkEnumerateInstanceExtensionProperties(nullptr, &extension_count, available_extensions.data());
      if(result != VK_SUCCESS) {
	spdlog::error("Could not enumerate extensions");
	throw;
      }

      // Print available extensions
      for(auto extension : available_extensions) {
	spdlog::info(" - {}", extension.extensionName);
      }

      // Info for application
      VkApplicationInfo app_info{
	.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
	.pApplicationName = application_name.c_str(),
	.pEngineName = engine_name.c_str(),
	.apiVersion = VK_API_VERSION_1_1
      };

      // Required extensions are set in platform options alongside main
      const std::vector<const char*> required_extensions = platform.get_required_extensions();
      spdlog::info("Enabling {} extensions", required_extensions.size());

      // Required layers defined at main
      std::vector<const char *> required_layers = platform.get_required_layers();
      spdlog::info("Enabling {} layers", required_layers.size());

      // Info for Instance
      VkInstanceCreateInfo instance_info{
	.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
	.pApplicationInfo = &app_info,
	.enabledLayerCount = static_cast<uint32_t>(required_layers.size()),
	.ppEnabledLayerNames = required_layers.data(),
	.enabledExtensionCount = static_cast<uint32_t>(required_extensions.size()),
	.ppEnabledExtensionNames = required_extensions.data()
      };

      // Create Instance
      result = vkCreateInstance(&instance_info, nullptr, &handle);
      if(result != VK_SUCCESS) {
	spdlog::error("Failed to create instance");
	throw;
      }

      // Volk requires loading the instance
      volkLoadInstance(handle);
      spdlog::info("Created instance");
    }

    // Destructor
    Instance::~Instance() {
      vkDestroyInstance(handle, nullptr);
      spdlog::info("Destroyed instance");
    }

    // Public method to access handle
    VkInstance Instance::get_handle() const {
      return handle;
    }
  }
}
