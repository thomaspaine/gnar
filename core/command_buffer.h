// Thomas Paine, 2024
// Command buffer header

#pragma once

//#include "common/helpers.h"

#include "core/device.h"
#include "core/swapchain.h"
#include "core/command_pool.h"
#include "core/render_pass.h"
#include "core/framebuffer.h"
#include "core/pipeline.h"
#include "core/descriptor_set.h"
#include "core/buffer_context.h"

#include "scene/scene_graph.h"

namespace gnar {
  namespace core {
    class CommandBuffer {
    public:
      struct DrawBuffer {
	uint32_t vertex_buffer_index;
	uint32_t index_buffer_index;
	uint32_t indices_count;
      };
      CommandBuffer(Device &device,
		    Swapchain &swapchain,
		    CommandPool &command_pool,
		    RenderPass &render_pass,
		    std::vector<DrawBuffer> &draw_buffers,
		    BufferContext &buffer_context,
		    scene::SceneGraph &scene_graph);
      VkCommandBuffer get_handle() const;
      void record(Framebuffer &framebuffer, Pipeline &pipeline, DescriptorSet &descriptor_set);
    private:
      VkCommandBuffer handle;
      Device &device;
      Swapchain &swapchain;
      RenderPass &render_pass;
      BufferContext &buffer_context;
      CommandPool &command_pool;

      std::vector<DrawBuffer> &draw_buffers;

      void copy_buffer_to_image(VkBuffer buffer, VkImage image, uint32_t width, uint32_t height);
      void transition_image_layout(VkImage image, VkFormat format, VkImageLayout from, VkImageLayout to);

      void begin();
      void end();
      void submit();
    };
  }
}
