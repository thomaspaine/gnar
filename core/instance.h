// Thomas Paine, 2024
// Instance header
// Instance is the crux of the vulkan API, interacting with vulkan headers and loaders
// VkInstance is responsible for loading layers and extensions
// Each platform requires a different instance surface extension
// Layers are used for api validation and api dumping

#pragma once

#include "common/includes.h"

#include "platform/application.h"

namespace gnar {
  namespace core {
    class Instance {
    public:
      Instance(const platform::Platform &platform,
	       const std::string &application_name,
	       const std::string &engine_name);
      ~Instance();
      VkInstance get_handle() const;
    private:
      uint32_t layer_count;
      uint32_t extension_count;
      std::vector<VkLayerProperties> available_layers;
      std::vector<VkExtensionProperties> available_extensions;

      VkInstance handle;
    };
  }
}
