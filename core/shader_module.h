// Thomas Paine, 2024
// Shader module header

#pragma once

//#include "common/helpers.h"

#include "core/device.h"

#include "platform/asset_manager_delegator.h"

namespace gnar {
  namespace core {
    class ShaderModule {
    public:
      ShaderModule(const std::string &filename, Device &device, platform::AssetManager &asset_manager);
      VkShaderModule get_handle() const;
    private:
      VkShaderModule handle;
      Device &device;
      platform::AssetManager &asset_manager;
      //std::vector<char> read_file(const std::string &filename);
    };
  }
}
