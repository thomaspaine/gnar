// Thomas Paine, 2024
// Command Pool wrapper

#include "command_pool.h"

namespace gnar {
  namespace core {
    CommandPool::CommandPool(Device &device) :
      device{device} {
      spdlog::info("Creating command pool");
      VkResult result;

      VkCommandPoolCreateInfo command_pool_info = {
	.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
	.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT
      };

      result = vkCreateCommandPool(device.get_handle(), &command_pool_info, nullptr, &handle);
      if(result != VK_SUCCESS) {
	spdlog::error("Could not create command pool");
	throw;
      }
      spdlog::info("Created command pool");
    }

    CommandPool::~CommandPool() {
      vkDestroyCommandPool(device.get_handle(), handle, nullptr);
      spdlog::info("Destroyed command pool");
    }

    VkCommandPool CommandPool::get_handle() const {
      return handle;
    }
  }
}
