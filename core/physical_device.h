// Thomas Paine, 2024
// Physical Device header
// This class is responsible for choosing the


#pragma once

#include "common/includes.h"

#include "core/instance.h"
#include "core/surface.h"

struct QueueFamilyIndices {
  uint32_t graphics_family;
  uint32_t present_family;
};

namespace gnar {
  namespace core {
    class PhysicalDevice {
    public:
      // initialiser
      PhysicalDevice(Instance &instance, Surface &surface);
      // get gpu
      VkPhysicalDevice get_handle() const;
      // get queue family properties
      const std::vector<VkQueueFamilyProperties> &get_queue_family_properties() const;
      // get gpu properties
      const VkPhysicalDeviceFeatures &get_device_features() const;
      const VkPhysicalDeviceProperties &get_physical_device_properties() const;
      const VkPhysicalDeviceMemoryProperties &get_memory_properties() const;
      const QueueFamilyIndices &get_indices() const;
    protected:
      // set handle
      void pick_gpu();
      // used to find discrete gpu TODO
      bool is_device_suitable(VkPhysicalDevice gpu);
    private:
      Instance &instance;
      Surface &surface;
      // gpu handle
      VkPhysicalDevice handle{VK_NULL_HANDLE};
      // gpu features
      VkPhysicalDeviceFeatures features;
      // gpu properties
      VkPhysicalDeviceProperties properties;
      // gpu memory properties
      VkPhysicalDeviceMemoryProperties memory_properties;
      // gpu queue family properties
      std::vector<VkQueueFamilyProperties> queue_family_properties;

      QueueFamilyIndices indices;
    };
  }
}
