// Thomas Paine, 2024
// Render pass header

#pragma once

//#include "common/helpers.h"

#include "core/device.h"

namespace gnar {
  namespace core {
    class RenderPass {
    public:
      RenderPass(Device &device, VkFormat &format);
      ~RenderPass();
      VkRenderPass get_handle() const;
    private:
      VkRenderPass handle;
      Device &device;
    };
  }
}
