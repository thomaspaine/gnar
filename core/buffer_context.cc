// Thomas Paine, 2024

#include "buffer_context.h"

namespace gnar {
  namespace core {
    BufferContext::BufferContext(Device &device) :
      device{device} {
      spdlog::info("Building buffer builder");
    }

    // void BufferContext::build_uniform_buffer(VkDeviceSize buffer_size) {
    //   uniform_buffer = std::make_unique<gnar::core::UniformBuffer>(device, buffer_size);
    // }

    uint32_t BufferContext::build_buffer(VkDeviceSize buffer_size, VkBufferUsageFlagBits usage) {
      buffers.push_back(std::make_unique<gnar::core::Buffer>(device, buffer_size, usage));
      return buffers.size() - 1;
    }

    // TODO fetch by index
    // UniformBuffer &BufferContext::get_uniform_buffer() {
    //   return *uniform_buffer;
    // }

    Buffer &BufferContext::get_buffer(uint32_t index) {
      return *buffers[index];
    }
  }
}
