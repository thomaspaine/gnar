// Thomas Paine, 2024
// Descriptor set wrapper

#include "descriptor_set.h"

namespace gnar {
  namespace core {
    DescriptorSet::DescriptorSet(Device &device,
				 BufferContext &buffer_context,
				 scene::SceneGraph &scene_graph,
				 uint32_t ubo_index) :
      device{device},
      uniform_buffer{buffer_context.get_buffer(ubo_index)}
    {
      spdlog::info("Creating descriptor set");
      VkResult result;

      VkDescriptorSetLayoutBinding ubo_layout_binding = {
	.binding = 0,
	.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
	.descriptorCount = 1,
	.stageFlags = VK_SHADER_STAGE_VERTEX_BIT
      };

      VkDescriptorSetLayoutBinding texture_layout_binding = {
	.binding = 1,
	.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
	.descriptorCount = 1,
	.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT
      };

      std::vector<VkDescriptorSetLayoutBinding> bindings = { ubo_layout_binding, texture_layout_binding };

      VkDescriptorSetLayoutCreateInfo descriptor_set_layout_info = {
	.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
	.bindingCount = static_cast<uint32_t>(bindings.size()),
	.pBindings = bindings.data()
      };

      result = vkCreateDescriptorSetLayout(device.get_handle(), &descriptor_set_layout_info, nullptr, &layout);
      if(result != VK_SUCCESS) {
	spdlog::error("Could not create descriptor set layout");
	throw;
      }
      spdlog::info("Created descriptor set layout");

      VkDescriptorPoolSize ubo_descriptor_pool_size = {
	.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
	.descriptorCount = 1 //frames_in_flight
      };
      VkDescriptorPoolSize sampler_descriptor_pool_size = {
	.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
	.descriptorCount = 1 //frames_in_flight
      };

      std::vector<VkDescriptorPoolSize> pool_sizes = { ubo_descriptor_pool_size, sampler_descriptor_pool_size };

      VkDescriptorPoolCreateInfo descriptor_pool_info = {
	.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
	.maxSets = 1, // frames in flight
	.poolSizeCount = static_cast<uint32_t>(pool_sizes.size()),
	.pPoolSizes = pool_sizes.data()
      };

      result = vkCreateDescriptorPool(device.get_handle(), &descriptor_pool_info, nullptr, &descriptor_pool);

      VkDescriptorSetAllocateInfo descriptor_set_info = {
	.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
	.descriptorPool = descriptor_pool,
	.descriptorSetCount = 1,
	.pSetLayouts = &layout
      };

      result = vkAllocateDescriptorSets(device.get_handle(), &descriptor_set_info, &handle);

      VkDescriptorBufferInfo ubo_buffer_info = {
	.buffer = uniform_buffer.get_handle(),
	.offset = 0,
	.range = uniform_buffer.get_size()
      };

      VkWriteDescriptorSet ubo_write = {
	.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
	.dstSet = handle,
	.dstBinding = 0,
	.dstArrayElement = 0,
	.descriptorCount = 1,
	.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
	.pBufferInfo = &ubo_buffer_info
      };

      std::vector<VkWriteDescriptorSet> writes = { ubo_write };

      uint32_t texture_count = scene_graph.get_texture_count();
      //for(uint32_t i = 0; i < texture_count; i++) {
      uint32_t i = 0; // TODO
	scene::Texture &texture = scene_graph.get_texture(i);

	VkDescriptorImageInfo &texture_info = texture.get_image_descriptor();

	VkWriteDescriptorSet texture_write = {
	  .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
	  .dstSet = handle,
	  .dstBinding = i + 1,
	  .dstArrayElement = 0,
	  .descriptorCount = 1,
	  .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
	  .pImageInfo = &texture_info
	};

	writes.push_back(texture_write);
	//}


      spdlog::info("Updating descriptor set");

      vkUpdateDescriptorSets(device.get_handle(), writes.size(), writes.data(), 0, nullptr);
      spdlog::info("Updated descriptor set");
    }

    DescriptorSet::~DescriptorSet() {
      vkDestroyDescriptorSetLayout(device.get_handle(), layout, nullptr);
      vkDestroyDescriptorPool(device.get_handle(), descriptor_pool, nullptr);
    }

    const VkDescriptorSet &DescriptorSet::get_handle() const {
      return handle;
    }
    // TODO move layout to seperate class
    const VkDescriptorSetLayout &DescriptorSet::get_layout() const {
      return layout;
    }
  }
}
