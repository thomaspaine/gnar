// Thomas Paine, 2024
// VkShaderModule wrapper

#include "shader_module.h"

namespace gnar {
  namespace core {
    ShaderModule::ShaderModule(const std::string &filename, Device &device, platform::AssetManager &asset_manager) :
      device{device}, asset_manager{asset_manager} {
      spdlog::info("Creating shader module: {}", filename);
      VkResult result;

      std::vector<char> data = asset_manager.read_file("shaders/" + filename);

      VkShaderModuleCreateInfo shader_module_info {
	.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
	.codeSize = data.size(),
	.pCode = reinterpret_cast<const uint32_t *>(data.data())
      };

      result = vkCreateShaderModule(device.get_handle(), &shader_module_info, nullptr, &handle);
      if(result != VK_SUCCESS) {
	spdlog::error("Could not create shader module");
	throw;
      }
      spdlog::info("Created shader module");
    }

    VkShaderModule ShaderModule::get_handle() const {
      return handle;
    }
  }
}
