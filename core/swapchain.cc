// Thomas Paine, 2024
// Create swapchain

#include "swapchain.h"

namespace gnar {
  namespace core {
    Swapchain::Swapchain(Device &device, Surface &surface) :
      device{device}, surface{surface} {
      spdlog::info("Creating swapchain");
      VkResult result;

      // Swapchain support details
      result = set_surface_formats();
      if(result != VK_SUCCESS) {
	spdlog::error("Could not set surface formats");
	throw;
      }
      choose_surface_format();
      spdlog::info("Chose surface format");

      // Present modes
      result = set_present_modes();
      if(result != VK_SUCCESS) {
	spdlog::error("Could not set present modes");
	throw;
      }
      choose_present_mode();
      spdlog::info("Set present modes");

      // Surface capabilities
      result = set_surface_capabilities();
      if(result != VK_SUCCESS) {
	spdlog::error("Could not set surface capabilities");
	throw;
      }
      spdlog::info("Set surface capabilities");

      // Extent
      choose_extent();

      VkSwapchainCreateInfoKHR create_info{
	.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
	.surface = surface.get_handle(),
	.minImageCount = surface_capabilities.minImageCount + 1,
	.imageFormat = surface_format.format,
	.imageColorSpace = surface_format.colorSpace,
	.imageExtent = extent,
	.imageArrayLayers = 1,
	.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
	.preTransform = surface_capabilities.currentTransform,
	.compositeAlpha = VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR,
	.presentMode = present_mode
      };

      result = vkCreateSwapchainKHR(device.get_handle(), &create_info, nullptr, &handle);
      if(result != VK_SUCCESS) {
	spdlog::error("Could not create swapchain");
	throw;
      }
      spdlog::info("Created swapchain.");

      uint32_t image_count;
      vkGetSwapchainImagesKHR(device.get_handle(), handle, &image_count, nullptr);
      images.resize(image_count);
      vkGetSwapchainImagesKHR(device.get_handle(), handle, &image_count, images.data());
    }

    Swapchain::~Swapchain() {
      vkDestroySwapchainKHR(device.get_handle(), handle, nullptr);
    }

    // surface formats
    VkResult Swapchain::set_surface_formats() {
      uint32_t format_count;
      vkGetPhysicalDeviceSurfaceFormatsKHR(device.get_gpu().get_handle(), surface.get_handle(), &format_count, nullptr);
      if(format_count < 1) {
	spdlog::error("No surface formats found");
	throw;
      }
      surface_formats.resize(format_count);
      return vkGetPhysicalDeviceSurfaceFormatsKHR(device.get_gpu().get_handle(), surface.get_handle(), &format_count, surface_formats.data());
    }

    // Present modes
    VkResult Swapchain::set_present_modes() {
      uint32_t present_modes_count;
      vkGetPhysicalDeviceSurfacePresentModesKHR(device.get_gpu().get_handle(), surface.get_handle(), &present_modes_count, nullptr);
      if(present_modes_count < 1) {
	spdlog::error("No surface present modes found");
	throw;
      }
      present_modes.resize(present_modes_count);
      return vkGetPhysicalDeviceSurfacePresentModesKHR(device.get_gpu().get_handle(), surface.get_handle(), &present_modes_count, present_modes.data());
    }

    // Surface capabilites
    VkResult Swapchain::set_surface_capabilities() {
      return vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device.get_gpu().get_handle(), surface.get_handle(), &surface_capabilities);
    }

    void Swapchain::choose_surface_format() {
      for(const auto &available_format : surface_formats) {
	if(available_format.format == VK_FORMAT_B8G8R8A8_SRGB
	   && available_format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
	  surface_format = available_format;
	  return;
	}
      }
      surface_format = surface_formats[0];
    }

    void Swapchain::choose_present_mode() {
      for(const auto &available_present_mode : present_modes) {
	if(available_present_mode == VK_PRESENT_MODE_MAILBOX_KHR) {
	  present_mode = available_present_mode;
	  return;
	}
      }
      present_mode = VK_PRESENT_MODE_FIFO_KHR;
    }

    void Swapchain::choose_extent() {
      //if(surface_capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
      extent = surface_capabilities.currentExtent;
#ifdef ANDROID
      uint32_t width = surface_capabilities.currentExtent.width;
      uint32_t height = surface_capabilities.currentExtent.height;
      spdlog::info("Choosing swap extent; w: {}, h: {}", width, height);
      if(surface_capabilities.currentTransform & VK_SURFACE_TRANSFORM_ROTATE_90_BIT_KHR ||
	 surface_capabilities.currentTransform & VK_SURFACE_TRANSFORM_ROTATE_270_BIT_KHR) {
	spdlog::info("Sideways in choose extent");
	//surface_capabilities.currentExtent.width = height;
	//surface_capabilities.currentExtent.height = width;
      }
      spdlog::info("extent chosen; w: {}, h: {}", surface_capabilities.currentExtent.width, surface_capabilities.currentExtent.height);
#endif
	//}
      // TODO
    }

    std::vector<VkImage> Swapchain::get_images() {
      return images;
    }

    VkFormat &Swapchain::get_format() {
      return surface_format.format;
    }

    VkExtent2D &Swapchain::get_extent() {
      return extent;
    }

    VkSurfaceTransformFlagBitsKHR Swapchain::get_transform() const {
      return surface_capabilities.currentTransform;
    }

    VkSwapchainKHR Swapchain::get_handle() const {
      return handle;
    }

    void Swapchain::recreate() {
      // TODO
      spdlog::error("Need to recreate swapchain");
    }
  }
}
