// Thomas Paine, 2024
// Wrapper for VkImageView

#include "image_view.h"

namespace gnar {
  namespace core {
    ImageView::ImageView(Device &device, VkImage &image_handle, VkFormat &format, VkImageAspectFlagBits flags) :
      device{device} {
      VkResult result;

      VkImageViewCreateInfo create_info{
	.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
	.image = image_handle,
	.viewType = VK_IMAGE_VIEW_TYPE_2D,
	.format = format,
	.subresourceRange{
	  .aspectMask = flags,
	  .levelCount = 1,
	  .layerCount = 1
	}
      };

      result = vkCreateImageView(device.get_handle(), &create_info, nullptr, &handle);

      if(result != VK_SUCCESS) {
	spdlog::error("Could not create image view");
	throw;
      }
      spdlog::info("Created image view");
    }
    ImageView::~ImageView() {
      vkDestroyImageView(device.get_handle(), handle, nullptr);
    }

    VkImageView ImageView::get_handle() const {
      return handle;
    }
  }
}
